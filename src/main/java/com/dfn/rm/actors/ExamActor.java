package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.actor.Cancellable;
import akka.actor.PoisonPill;
import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.actors.messages.*;
import com.dfn.rm.beans.*;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import com.dfn.rm.connector.fileSystem.FileSystem;
import com.dfn.rm.connector.fileSystem.PaperPDF;
import com.dfn.rm.connector.websocket.MessageType;
import com.dfn.rm.connector.websocket.messages.*;
import com.dfn.rm.email.ContactMail;
import com.dfn.rm.email.mailsender.MailSenderSwingWorker;
import com.dfn.rm.exam.ExamPaperFactory;
import com.dfn.rm.util.*;
import com.dfn.rm.util.json.JSONParser;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import scala.concurrent.duration.Duration;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * This actor is responsible for a handling each and every operation of exams once
 * client notifies about exams. Actor should persist it's state.
 */
public class ExamActor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(ExamActor.class);

    private final ScheduleExam scheduleExam;
    private ExamPaper examPaper;
    private double totalMarks;

    private boolean isExamStarted;
    private long secondCount;
    private boolean isDurationOver;

    private List<Cancellable> continuousScheduleService = new ArrayList<>();
    private ExamDetails examDetails;

    public ExamActor(LoginResponse loginResponse) {
        this.scheduleExam = loginResponse.getScheduleExam();
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        long durationSeconds =
                Math.round((Instant.now().toEpochMilli() - scheduleExam.getExpireDate().getTime()) / 1000.0);
        getContext().system().scheduler().scheduleOnce(
                Duration.create(durationSeconds, TimeUnit.SECONDS),
                getSelf(),
                new ExamExpire(),
                getContext().system().dispatchers().lookup("schedule-event-dispatcher"),
                getSelf());
        recoverActorSnapshot(scheduleExam.getUserId());
    }

    private void recoverActorSnapshot(String userID) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {

            try {
                List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                        userID, handle);
                ExamDetails examDetails = null;
                if (examDetailsList.size() > 0) {
                    examDetails = examDetailsList.get(0);
                }
                ExamActorRecovery examActorRecovery = DBHandler.getInstance().getExamActorSnapShot(
                        examDetails.getExamId(), userID, handle);
                if (examActorRecovery != null) {
                    byte[] data = examActorRecovery.getPaperRecoveryByte();
                    examPaper = SerializationUtils.deserialize(data);
                    if (examActorRecovery.getOverlyElapsed() == Constants.ON_DURATION) {
                        isDurationOver = false;
                    } else {
                        isDurationOver = true;
                    }
                    secondCount = examActorRecovery.getElapsedTime();
                    isExamStarted = true;
                }
            } catch (Exception e) {
                LOGGER.error("Error When recovering Actor SnapShot From DB ", e);
            }
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }


    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(ExamStart.class, this::handle)
                .match(RemainingTime.class, this::handle)
                .match(ExamEnd.class, this::handle)
                .match(RequestMessage.class, this::handleRequestMessage)
                .match(LoginResponse.class, this::handle)
                .match(PauseActiveExam.class, this::handle)
                .build();
    }

    public void handle(PauseActiveExam pauseActiveExam) {
        LOGGER.trace("Exam Paused {}" + JSONParser.getInstance().toJson(pauseActiveExam));
        stopSchedulerService();
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {

            try {
                if (isDurationOver()) {
                    DBHandler.getInstance().updateExamActorRecoverySecondary(secondCount,
                            Constants.OFF_DURATION, scheduleExam.getUserId(), scheduleExam.getExamId(), handle);
                } else {
                    DBHandler.getInstance().updateExamActorRecovery(secondCount,
                            scheduleExam.getUserId(), scheduleExam.getExamId(), handle);
                }
            } catch (Exception e) {
                LOGGER.error("Error During Pausing the Active Exam", e);
            }

        } catch (Exception e) {
            LOGGER.error("Error {} ", e, e);
        }
    }

    public void handle(LoginResponse loginResponse) {
        LOGGER.trace("Login Response Received {}" + JSONParser.getInstance().toJson(loginResponse));
        if (isExamStarted()) {
            sendMessage(loginResponse, MessageType.RESPONSE_MSG_LOGIN_SECONDARY);
            recoverActorSnapshot(scheduleExam.getUserId());
            sendMessage(getExamPaper(), MessageType.RESPONSE_MSG_EXAM_PAPER_GENERATED);
            handleMarkedAnswers();
            startSchedulerService();
        } else {
            handleScheduleExam(loginResponse);
        }
    }

    public void handleMarkedAnswers() {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            try {
                List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                        scheduleExam.getUserId(), handle);
                if (examDetailsList.size() > 0) {
                    examDetails = examDetailsList.get(0);
                }

                List<ExamAnswer> examAnswers = DBHandler.getInstance().getMarkedAnswers(
                        scheduleExam.getUserId(), examDetails.getPaperId(), handle);
                if (examAnswers != null) {
                    TickedExamPaper markedExamPaper = PaperUtils.generateMarkedQuestions(examAnswers);
                    sendMessage(markedExamPaper, MessageType.RESPONSE_MSG_MARKED_EXAM_QUESTIONS);
                }
            } catch (Exception e) {
                LOGGER.error("Error During Processing Marked Answers of paper", e);
            }
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    public void handleScheduleExam(LoginResponse loginResponse) {

        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {

            try {
                List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                        scheduleExam.getUserId(), handle);

                ExamDetails examDetails = examDetailsList.get(0);
                ExamAgreement examAgreement = new ExamAgreement(
                        examDetails.getPosition(),
                        DateUtils.generateDuration(examDetails.getDuration()),
                        examDetails.getExamId() + "",
                        Constants.DEF_EXAM_DESCRIPTION,
                        Constants.DEF_EXAM_HONOR_CODE, examDetails.getNoOfQuestions()
                );
                loginResponse.setExamAgreement(examAgreement);
                sendMessage(loginResponse, MessageType.REQUEST_MSG_LOGIN);
            } catch (Exception e) {
                LOGGER.error("Error During handling the Schedule Exam Operation", e);
            }

        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }

    }

    public Answer processAnswers(Question question, Object selectedAnswer, List<Answer> allAnswerList) {

        Answer answer = null;
        try {
            List<Answer> answers = allAnswerList.stream().filter(
                    x -> x.getQuestionId() == question.getId()).collect(Collectors.toList());

            answer = PaperUtils.filterAnswerList(answers, selectedAnswer, question.getCategory());
        } catch (Exception e) {
            LOGGER.error("Error {} ", e, e);
            answer = new Answer();
        }
        return answer;
    }

    private void handleMarkExamPaper(RequestMessage requestMessage) {
        LOGGER.trace("Exam Paper Submitted and Received {}" + JSONParser.getInstance().toJson(requestMessage));
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            stopSchedulerService();
            double marks = 0;

            MarkedExamPaper examPaper = Utils.getMessageInstance(requestMessage.getData(), MarkedExamPaper.class);
            if (examPaper != null) {
                List<Question> allQuestionsList = DBHandler.getInstance().getQuestions(handle);
                List<Answer> allAnswersList = DBHandler.getInstance().getAllAnswersWithWeight(handle);
                List<MarkedQuestion> questions = examPaper.getQuestions();
                if (questions != null) {
                    for (MarkedQuestion markedQuestion : questions) {
                        Question question = allQuestionsList.stream().filter(
                                x -> markedQuestion.getQuestionID() == x.getId())
                                .findAny()
                                .orElse(null);
                        List<Object> answers = markedQuestion.getAnswerIDs();
                        if (answers != null) {
                            for (Object markedAnswer : answers) {
                                if (question.getCategory() == Constants.QUESTION_TYPE_SHORT_ANSWERS) {
                                    List<Object> list = Arrays.asList(((String) markedAnswer).replaceAll(
                                            "\\s", "").toLowerCase().split(","));
                                    for (Object obj : list) {
                                        Answer answer = processAnswers(question, obj, allAnswersList);
                                        marks += answer.getWeight();
                                    }
                                } else {
                                    Answer answer = processAnswers(question, markedAnswer, allAnswersList);
                                    marks += answer.getWeight();
                                }

                            }
                        }
                    }
                }
            }

            setTotalMarks(marks);
            handleSecondaryTasks(requestMessage.getHeader().getUserId(), getTotalMarks());
            sendMessage(null, MessageType.RESPONSE_MSG_EXAM_END);
            getSelf().tell(PoisonPill.getInstance(), getSelf());

        } catch (Exception e) {
            LOGGER.error("Error When Marking the Exam Paper", e);
        }

    }

    public boolean generatePaperPdf(Handle handle, String userId, double marks) {
        boolean status = true;
        try {
            List<Question> paperQuestionList = null;
            List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                    userId, handle
            );
            if (examDetailsList.size() > 0) {
                ExamDetails examDetails = examDetailsList.get(0);
                List<Question> allQuestions = DBHandler.getInstance().getQuestions(handle);
                List<Answer> allAnswers = DBHandler.getInstance().getAllAnswers(handle);
                List<ExamAnswer> examAnswers = DBHandler.getInstance().getMarkedAnswers(
                        userId, examDetails.getPaperId(), handle);

                if (examAnswers != null) {
                    TickedExamPaper markedExamPaper = PaperUtils.generateMarkedQuestions(examAnswers);
                    if (markedExamPaper != null) {
                        paperQuestionList = PaperUtils.generatePaperQuestionList(markedExamPaper,
                                allQuestions, allAnswers);

                        PaperPDF paperPDF = new PaperPDF();
                        status = paperPDF.createPDF(paperQuestionList, "DFN_" + userId, marks);

                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
        return status;
    }

    public void updateExamsTable(String userId, double marks, Handle handle) {
        try {
            boolean status = generatePaperPdf(handle, userId, marks);
            DBHandler.getInstance().updateExams(userId, Constants.EXAM_FINISHED, marks,
                    FileSystem.getPaperFilePath("DFN_" + userId), handle);
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    public void handleSecondaryTasks(String userId, double marks) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            sendMailToPanel(userId, marks, handle);
            updateExamsTable(userId, marks, handle);
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    public void sendMailToPanel(String userID, double marks, Handle handle) {
        try {
            UserRegistration userRegistration = DBHandler.getInstance().getUserRegistration(userID, handle);
            Map map = MailUtils.addToPanelMap(userRegistration, marks);

            ContactMail contactMail = new ContactMail();
            contactMail.setModel(map);
            contactMail.setEmailList(UserRegUtils.generatePanelMailList(userRegistration.getTechIntviewPanel()));
            MailSenderSwingWorker mailSenderSwingWorker = new MailSenderSwingWorker(contactMail);
            mailSenderSwingWorker.execute();
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }

    }

    private void handleRequestMessage(RequestMessage requestMessage) {
        LOGGER.trace(JSONParser.getInstance().toJson(requestMessage));
        int messageType = requestMessage.getHeader().getMessageType();
        switch (messageType) {
            case MessageType.REQUEST_MSG_CACHING_EXAM_ANSWERS:
                handleCachingExamAnswers(requestMessage.getData());
                break;
            case MessageType.REQUEST_MSG_MARK_PAPER:
                handleMarkExamPaper(requestMessage);
                break;
            default:
                break;
        }

    }

    private void handleCachingExamAnswers(Object messaage) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {

            try {
                Entry entry = Utils.getMessageInstance(messaage, Entry.class);
                if (examDetails == null) {
                    List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                            scheduleExam.getUserId(), handle
                    );
                    if (examDetailsList.size() > 0) {
                        examDetails = examDetailsList.get(0);
                    }
                }
                ExamAnswer examAnswer = new ExamAnswer(scheduleExam.getUserId(),
                        examDetails.getPaperId(), entry.getQuestionID(), entry.getAnswerID());

                if (entry.getCategory() != Constants.QUESTION_TYPE_SHORT_ANSWERS) {
                    if (entry.isChecked) {
                        DBHandler.getInstance().addToExamAnswer(examAnswer, handle);
                    } else {
                        DBHandler.getInstance().deleteFromExamAnswer(examAnswer, handle);
                    }
                } else {
                    ExamAnswer previousExamAnswer = DBHandler.getInstance().getQuestionByID(scheduleExam.getUserId(),
                            examDetails.getPaperId(), entry.getQuestionID(), handle);
                    if (previousExamAnswer == null) {
                        DBHandler.getInstance().addToExamAnswer(examAnswer, handle);
                    } else {
                        DBHandler.getInstance().updateExamAnswer(examAnswer, handle);
                    }
                }

            } catch (Exception e) {
                LOGGER.error("Error when Adding Exam Answer to handling Tick Untick", e);
            }

        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    private void handle(ExamEnd examEnd) {

    }

    private void handle(RemainingTime remainingTime) {
        String time = null;

        if (secondCount == 0 || isDurationOver) {
            if (!isDurationOver) {
                isDurationOver = true;
            }
            time = DateUtils.generateTime(secondCount);
            secondCount++;
        }

        if (!isDurationOver) {
            time = DateUtils.generateTime(secondCount);
            secondCount--;
        }
        sendMessage(new RemainingTime(time, isDurationOver), MessageType.RESPONSE_MSG_EXAM_TIME_REMAIN);
    }

    private void handlePaperGenerator() {

        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().installPlugins().open()) {
            List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                    scheduleExam.getUserId(), handle
            );
            if (examDetailsList.size() > 0) {
                ExamDetails examDetails = examDetailsList.get(0);
                ExamPaperFactory examPaperFactory = new ExamPaperFactory(
                        examDetails.getExamType(), examDetails.getPosition(), examDetails.getExamCategory());
                examPaper = examPaperFactory.generateExamPaper();
                if (getExamPaper() != null) {
                    saveExamPaperSnapShotDB(examDetails.getPaperId());
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    private void saveExamPaperSnapShotDB(String paperID) {
        if (getExamPaper() != null) {
            try (Handle handle = DBUtilStore.getInstance().getDbiInstance().installPlugins().open()) {
                byte[] data = SerializationUtils.serialize(getExamPaper());
                ExamActorRecovery examActorRecovery = new ExamActorRecovery(scheduleExam.getUserId(), paperID,
                        scheduleExam.getExamId(), secondCount, Constants.ON_DURATION, data);
                DBHandler.getInstance().addToExamPaperRecovery(examActorRecovery, handle);
            } catch (Exception e) {
                LOGGER.error("Error {}", e, e);
            }
        }
    }

    public void stopSchedulerService() {
        getContinuousScheduleService().forEach(Cancellable::cancel);
        LOGGER.info("Continuous Schedulers Cancellable Events {}", getContinuousScheduleService().size());
    }

    private void startSchedulerService() {
        Cancellable continuousEvent = getContext().system().scheduler().scheduleWithFixedDelay(
                Duration.create(0, TimeUnit.SECONDS),
                Duration.create(1000, TimeUnit.MILLISECONDS),
                getSelf(),
                new RemainingTime(),
                getContext().system().dispatchers().lookup("schedule-event-dispatcher"),
                getSelf());

        getContinuousScheduleService().add(continuousEvent);
    }

    private void handle(ExamStart examStart) {
        handlePaperGenerator();
        if (getExamPaper() != null) {
            isExamStarted = true;
            secondCount = DateUtils.convertToSeconds(scheduleExam.getDuration());
            sendMessage(getExamPaper(), MessageType.RESPONSE_MSG_EXAM_PAPER_GENERATED);
            startSchedulerService();
        }
    }

    public void sendMessage(Object messege, int messageType) {
        ResponseMessage responseMessage = new ResponseMessage();
        ResponseHeader responseHeader = new ResponseHeader();
        responseHeader.setUserId(scheduleExam.getUserId());
        responseHeader.setMessageType(messageType);
        if (messege != null) {
            responseMessage.setResObject(messege);
        }
        responseMessage.setHeader(responseHeader);

        try {
            LOGGER.trace(JSONParser.getInstance().toJson(responseMessage));
            ActorUtils.getClientSupervisor(getContext()).tell(responseMessage, getSelf());
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    public ExamPaper getExamPaper() {
        return examPaper;
    }

    public double getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(double totalMarks) {
        this.totalMarks = totalMarks;
    }

    public boolean isExamStarted() {
        return isExamStarted;
    }

    public boolean isDurationOver() {
        return isDurationOver;
    }

    public List<Cancellable> getContinuousScheduleService() {
        return continuousScheduleService;
    }
}
