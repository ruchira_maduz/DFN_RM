package com.dfn.rm.actors;

import akka.actor.*;
import akka.japi.pf.DeciderBuilder;
import com.dfn.rm.actors.messages.PauseActiveExam;
import com.dfn.rm.actors.messages.ScheduleExam;
import com.dfn.rm.beans.ExamDetails;
import com.dfn.rm.beans.User;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import com.dfn.rm.connector.websocket.ClientUtils;
import com.dfn.rm.connector.websocket.EventServer;
import com.dfn.rm.connector.websocket.EventSocket;
import com.dfn.rm.connector.websocket.MessageType;
import com.dfn.rm.connector.websocket.events.*;
import com.dfn.rm.connector.websocket.messages.*;
import com.dfn.rm.util.ActorName;
import com.dfn.rm.util.ActorUtils;
import com.dfn.rm.util.Constants;
import com.dfn.rm.util.json.JSONParser;
import com.dfn.rm.util.security.PasswordEncrypter;
import com.dfn.rm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.common.WebSocketSession;
import org.jdbi.v3.core.Handle;

import java.io.IOException;
import java.time.Duration;
import java.util.*;

import static akka.actor.SupervisorStrategy.resume;


/**
 * Client supervisor will act as the main endpoint which Recutement Manager communicates with client applications.
 * Whenever a client connects, it looks up if an ongoing session exists between client and Recutement Manager.
 * When a message is received, Recutement Manager looks up if a Client Actor exists for that message, by extracting the
 * client ID
 * from the message header. If exists, it will route the web socket message body to that client actor.
 * If there's no session (no client actor) Recutement Manager will send a session reset message back to the client.
 * In order to create a new session, the client should send a login request to Athena. When a Login request is
 * received, Recutement Manager will authenticate the user and create a new Client Actor, to manage the session.
 * The Client Actor will maintain a session by sending keep-alive messages between Recutement Manager and the client,
 * Further,
 * Client Actor manages push1 messages to that specific client. This will simplify session management when multiple
 * clients are connected.
 * Recutement Manager will support, multi-user, multi-client web request management.
 */

public class ClientSupervisor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(ClientSupervisor.class);
    private static final int MAX_NO_OF_RETRIES = 1;
    private static final int DURATION = 8;
    private static SupervisorStrategy strategy =
            new OneForOneStrategy(
                    MAX_NO_OF_RETRIES,
                    Duration.ofSeconds(DURATION),
                    DeciderBuilder.match(Exception.class, e -> resume())
                            .build());


    private Map<String, SessionInfo> userIdSessionMap = new HashMap<>();
    private List<Integer> restrictedRequests = new ArrayList<>();


    @Override
    public void preStart() throws Exception {
        super.preStart();
        LOGGER.info("Starting Client Supervisor");
        int port = Integer.parseInt(Settings.getInstance().getClientSettings().getPort());
        String endpointPath = Settings.getInstance().getClientSettings().getEndpoint();
        List<Integer> restrictedRequestSettings = Settings.getInstance().getClientSettings().getRestrictedRequests();
        restrictedRequests.addAll(restrictedRequestSettings != null ? restrictedRequestSettings : new ArrayList<>());
        try {
            Runnable eventServerThread = () -> EventServer.startWSServer(port, endpointPath);
            new Thread(eventServerThread).start();
            EventSocket.setClientSupervisor(getContext());
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SessionConnectEvent.class, this::handleSessionConnectEvent)
                .match(SessionDisconnectEvent.class, this::handleSessionDisconnectEvent)
                .match(SessionCloseEvent.class, this::handleSessionCloseEvent)
                .match(WSMessageEvent.class, this::handleWSMessageEvent)
                .match(ResponseMessage.class, this::handleResponseMessage)
                .build();
    }

    private void handleResponseMessage(ResponseMessage responseMessage) {
        ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                responseMessage.getHeader().getUserId())
                .tell(responseMessage, getSelf());
    }

    private void handleSessionConnectEvent(SessionConnectEvent sessionConnectEvent) {
        LOGGER.info("Session Connected: {}", sessionConnectEvent.getSession());
    }

    private void handleSessionCloseEvent(SessionCloseEvent sessionCloseEvent) {
        LOGGER.warn("Session Closing! : {}", sessionCloseEvent.getSession());
        try {
            List<SessionInfo> sessionInfoList = new ArrayList<>(userIdSessionMap.values());
            for (SessionInfo sessionInfo : sessionInfoList) {
                if (getSessionObjectID(sessionInfo.getSession())
                        .equals(getSessionObjectID(sessionCloseEvent.getSession()))) {
                    cleanUpPreviousSession(sessionInfo.getUserID());
                    // kill session actor if actor alive
                    ActorRef actorRef = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                            sessionInfo.getUserID());
                    if (actorRef != null) {
                        actorRef.tell(PoisonPill.getInstance(), getSelf());
                    }

                    ActorUtils.getExamsSupervisor(getContext())
                            .tell(new PauseActiveExam(sessionInfo.getUserID()), getSelf());
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleSessionDisconnectEvent(SessionDisconnectEvent sessionDisconnectEvent) {
        try {
            String userID = sessionDisconnectEvent.getSessionInfo().getUserID();
            LOGGER.debug("Session Timed out.: {}", userID);
            cleanUpPreviousSession(userID);
            // kill session actor if actor alive
            ActorRef actorRef = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                    userID);
            if (actorRef != null) {
                actorRef.tell(PoisonPill.getInstance(), getSelf());
            }

            ActorUtils.getExamsSupervisor(getContext())
                    .tell(new PauseActiveExam(userID), getSelf());

        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleWSMessageEvent(WSMessageEvent wsMessageEvent) {
        LOGGER.trace("Message Received. WS Message: {}, WS Session: {}", wsMessageEvent.getMessage(),
                wsMessageEvent.getSession());
        try {
            RequestMessage requestMessage = JSONParser.getInstance().fromJson(wsMessageEvent.getMessage(),
                    RequestMessage.class);
            handleRequestMessage(requestMessage, wsMessageEvent.getSession());
        } catch (Exception e) {
            LOGGER.error("Error while processing the message:{}", wsMessageEvent.getMessage(), e);
        }

    }

    private void handleRequestMessage(RequestMessage requestMessage, Session session) {
        try {
            int messageType = requestMessage.getHeader().getMessageType();
            boolean isSessionValid = true;
            if (messageType == MessageType.REQUEST_MSG_LOGIN) {
                isSessionValid = validateLogin(requestMessage.getHeader());
            } else if (messageType == MessageType.REQUEST_MSG_PULSE) {
                SessionInfo sessionInfo = userIdSessionMap.get(requestMessage.getHeader().getUserId());
                if (sessionInfo == null) {
                    isSessionValid = false;
                } else {
                    isSessionValid = sessionInfo.isAuthenticated();
                }
            } else if (requestMessage.getHeader().getUserId() == null
                    || requestMessage.getHeader().getUserId().equals("")) {
                isSessionValid = false;
            } else {
                isSessionValid = validateSession(requestMessage.getHeader(), session);
            }
            if (isSessionValid) {
                switch (messageType) {
                    case MessageType.REQUEST_MSG_PULSE:
                        handlePulseMessage(requestMessage);
                        break;
                    case MessageType.REQUEST_MSG_LOGIN:
                        handleLoginRequestMessage(requestMessage, session);
                        break;
                    default:
                        handleClientRequestMessage(requestMessage, session);
                        break;
                }
            } else {
                LOGGER.warn("Invalid request: {}, Session: {}",
                        requestMessage, session);
                ClientUtils.sendErrorResponse(requestMessage.getHeader(), session, HttpStatus.UNAUTHORIZED_401);
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleClientRequestMessage(RequestMessage requestMessage, Session session) {
        try {
            SessionInfo sessionInfo = userIdSessionMap.get(requestMessage.getHeader().getUserId());
            ActorRef sessionActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                    sessionInfo.getUserID());
            if (sessionActor != null) {
                sessionActor.tell(requestMessage, getSelf());
                LOGGER.trace("Telling Session Actor: {}",
                        JSONParser.getInstance().toJson(requestMessage));
            } else {
                ClientUtils.sendErrorResponse(requestMessage.getHeader(), session, HttpStatus.BAD_REQUEST_400);
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handlePulseMessage(RequestMessage requestMessage) {
        SessionInfo sessionInfo = userIdSessionMap.get(requestMessage.getHeader().getUserId());
        if (sessionInfo != null) {
            ActorRef actorRef = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                    sessionInfo.getUserID());
            if (actorRef != null) {
                actorRef.tell(requestMessage, getSelf());
            }
        }
    }

    private void handleLoginRequestMessage(RequestMessage requestMessage, Session session) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            LoginRequest loginRequest = ClientUtils.getMessageInstance(requestMessage.getData(), LoginRequest.class);
            LoginResponse loginResponse = new LoginResponse();
            ResponseHeader responseHeader = ResponseHeader.getNewInstance(requestMessage.getHeader());

            User user = DBHandler.getInstance().getUser(loginRequest.getUsrname(), handle);

            // check login info is valid
            if (validateLogin(loginRequest, user)) {
                // clean up previous session
                cleanUpPreviousSession(loginRequest.getUsrname());

                // create session info object
                String sessionID = generateSessionID();
                requestMessage.getHeader().setSessionId(sessionID);
                SessionInfo sessionInfo = new SessionInfo(sessionID, session);
                sessionInfo.setClientIP(requestMessage.getHeader().getClientIp());
                sessionInfo.setUserID(loginRequest.getUsrname());
                sessionInfo.setTenantCode(requestMessage.getHeader().getTenantCode());
                sessionInfo.setCommVersion(requestMessage.getHeader().getClientVersion());

                // create session actor
                ActorRef clientActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                        loginRequest.getUsrname());
                if (clientActor == null) {
                    getContext().actorOf(Props.create(ClientActor.class, sessionInfo)
                                    .withDispatcher("client-dispatcher"),
                            ActorName.Client_ACTOR.toString()
                                    + "-"
                                    + loginRequest.getUsrname());
                }

                if (clientActor != null) {
                    clientActor.tell(sessionInfo, getSelf());
                }

                loginResponse.setUserType(user.getUserType());
                loginResponse.setName(user.getFullName());

                boolean l2Validated = true;
                boolean l3validated = true;

                userIdSessionMap.put(loginRequest.getUsrname(), sessionInfo);
                sessionInfo.setUserType(user.getUserType());
                responseHeader.setUserId(user.getUserName());
                responseHeader.setSessionId(sessionID);

                // populate response params
                if (user.getUserType() == Constants.USER_CAT_EXAM) {
                    List<ExamDetails> examDetailsList = DBHandler.getInstance().getExamDetails(
                            loginRequest.getUsrname(), handle
                    );

                    if (examDetailsList.size() > 0) {
                        ExamDetails examDetails = examDetailsList.get(0);

                        if (examDetails.getStatus() == Constants.EXAM_ON_HOLD) {
                            loginResponse.setAuthStatus(Constants.LOGIN_SUCCESS);

                            ScheduleExam scheduleExam = new ScheduleExam(new Date(),
                                    examDetails.getDuration(),
                                    examDetails.getExamId(),
                                    user.getUserName(),
                                    examDetails.getExamType(),
                                    examDetails.getPosition());

                            loginResponse.setScheduleExam(scheduleExam);
                            ActorUtils.getExamsSupervisor(getContext())
                                    .tell(loginResponse, getSelf());
                        } else if (examDetails.getStatus() == Constants.EXAM_FINISHED) {
                            l3validated = false;
                        }

                    } else {
                        l2Validated = false;
                    }
                }

                if (l2Validated) {
                    sessionInfo.setAuthenticated(true);
                }
                if (l2Validated && user.getUserType() == Constants.USER_CAT_ADMIN) {
                    loginResponse.setAuthStatus(Constants.LOGIN_SUCCESS);
                    sendResponseMessage(responseHeader, loginResponse, session);
                } else if (!l2Validated) {
                    sessionInfo.setAuthenticated(false);
                    loginResponse.setAuthStatus(Constants.LOGIN_RESTRICTED);
                    sendResponseMessage(responseHeader, loginResponse, session);
                } else if (!l3validated) {
                    sessionInfo.setAuthenticated(false);
                    loginResponse.setAuthStatus(Constants.LOGIN_EXPIRED);
                    sendResponseMessage(responseHeader, loginResponse, session);
                }

            } else {
                loginResponse.setAuthStatus(Constants.LOGIN_FAILED);
                ActorRef childActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                        loginRequest.getUsrname());
                if (childActor != null) {
                    childActor.tell(PoisonPill.getInstance(), getSelf());
                }
                sendResponseMessage(responseHeader, loginResponse, session);
            }
        } catch (Exception e) {
            LOGGER.error("Error: ", e);
            ClientUtils.sendErrorResponse(requestMessage.getHeader(), session, HttpStatus.BAD_REQUEST_400);
        }
    }

    public void sendResponseMessage(ResponseHeader responseHeader, Object object, Session session) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setHeader(responseHeader);
        responseMessage.setResObject(object);
        sendMessage(responseMessage, session);
    }

    private void sendMessage(ResponseMessage responseMessage, Session session) {
        ActorRef sessionActor = ActorUtils.getChildActor(getContext(), ActorName.Client_ACTOR,
                responseMessage.getHeader().getUserId());
        if (sessionActor != null) {
            sessionActor.tell(responseMessage, getSelf());
            LOGGER.debug("Telling Session Actor: {}", sessionActor);
        } else {
            try {
                LOGGER.trace(JSONParser.getInstance().toJson(responseMessage));
                session.getRemote().sendString(JSONParser.getInstance()
                        .toJson(responseMessage));
            } catch (IOException e) {
                LOGGER.error("Error in sending message {}", responseMessage.getHeader().getUserId());
            }
            LOGGER.error("Session Actor not found! {}", responseMessage.getHeader().getUserId());
        }
    }

    private boolean validateLogin(RequestHeader requestHeader) {
        boolean isValid = true;
        SessionInfo sessionInfo = userIdSessionMap.get(requestHeader.getUserId());
        if (sessionInfo != null) {
            isValid = !sessionInfo.isAuthenticated();
        }
        return isValid;
    }

    private boolean validateSession(RequestHeader requestHeader, Session session) {
        boolean isValid;
        SessionInfo sessionInfo = userIdSessionMap.get(requestHeader.getUserId());
        if (sessionInfo == null) {
            isValid = false;
        } else {
            isValid = sessionInfo.isAuthenticated();
            if (!requestHeader.getSessionId().equals(sessionInfo.getSessionID())) {
                isValid = false;
            }
            if (!requestHeader.getClientIp().equals(sessionInfo.getClientIP())) {
                isValid = false;
            }
            if (restrictedRequests.contains(requestHeader.getMessageType())) {
                isValid = sessionInfo.getUserType() == Constants.USER_CAT_ADMIN;
            }
        }
        return isValid;
    }

    private void cleanUpPreviousSession(String userID) {
        SessionInfo sessionInfo = userIdSessionMap.get(userID);
        if (sessionInfo != null) {
            // remove session
            userIdSessionMap.remove(userID);
            // close old session
            if (sessionInfo.getSession().isOpen()) {
                sessionInfo.getSession().close();
            }

        }

    }

    private boolean validateLogin(LoginRequest loginRequest, User user) {
        boolean isValid = true;
        if (user == null) {
            isValid = false;
        } else {
            if (!user.getPasswordHash()
                    .equals(PasswordEncrypter
                            .getsha256Securepassword(loginRequest.getPassword(),
                                    user.getSalt().getBytes()))) {
                isValid = false;
            }
        }
        return isValid;
    }

    private String getSessionObjectID(Session session) {
        return ((WebSocketSession) session).getConnection().getId();
    }

    private String generateSessionID() {
        return (UUID.randomUUID()).toString();
    }
}
