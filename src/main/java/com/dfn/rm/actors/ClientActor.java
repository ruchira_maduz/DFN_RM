package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.actors.messages.ExamStart;
import com.dfn.rm.actors.messages.StartSearch;
import com.dfn.rm.beans.*;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import com.dfn.rm.connector.fileSystem.FileAssembler;
import com.dfn.rm.connector.fileSystem.FileSystem;
import com.dfn.rm.connector.websocket.ClientUtils;
import com.dfn.rm.connector.websocket.MessageType;
import com.dfn.rm.connector.websocket.events.SessionDisconnectEvent;
import com.dfn.rm.connector.websocket.events.SessionInfo;
import com.dfn.rm.connector.websocket.events.SessionTimeOutEvent;
import com.dfn.rm.connector.websocket.messages.RequestHeader;
import com.dfn.rm.connector.websocket.messages.RequestMessage;
import com.dfn.rm.connector.websocket.messages.ResponseHeader;
import com.dfn.rm.connector.websocket.messages.ResponseMessage;
import com.dfn.rm.datastore.RequestHeaderDataStore;
import com.dfn.rm.email.ContactMail;
import com.dfn.rm.email.mailsender.RegistrationMailSenderSwingWorker;
import com.dfn.rm.util.*;
import com.dfn.rm.util.json.JSONParser;
import com.dfn.rm.util.settings.Settings;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;

import java.io.IOException;
import java.time.Duration;

/**
 * Actor for handling activities between a client and RecrutementManager .
 */

public class ClientActor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(ClientActor.class);

    protected SessionInfo sessionInfo;

    private Cancellable sessionTimeout;

    private FileAssembler fileAssembler;

    public ClientActor(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()
                .match(RequestMessage.class, this::handleRequestMessage)
                .match(ResponseMessage.class, this::handleResponseMessage)
                .match(SessionTimeOutEvent.class, this::handleSessionTimeOut)
                .build();
    }

    private void sendMessage(ResponseMessage responseMessage) {
        responseMessage.getHeader().setSessionId(sessionInfo.getSessionID());
        sendMessageToSession(responseMessage);
    }

    private void handleResponseMessage(ResponseMessage responseMessage) {
        LOGGER.trace(JSONParser.getInstance().toJson(responseMessage));
        int messageType = responseMessage.getHeader().getMessageType();
        switch (messageType) {
            case MessageType.RESPONSE_MSG_LOGIN:
            case MessageType.RESPONSE_MSG_EXAM_PAPER_GENERATED:
            case MessageType.RESPONSE_MSG_EXAM_TIME_REMAIN:
            case MessageType.RESPONSE_MSG_LOGIN_SECONDARY:
            case MessageType.RESPONSE_MSG_EXAM_END:
            case MessageType.RESPONSE_MSG_MARKED_EXAM_QUESTIONS:
            case MessageType.RESPONSE_MSG_STUDENT_SEARCH_BY_PAGINATION:
            case MessageType.RESPONSE_MSG_STUDENT_SEARCH_INITIATED:
            case MessageType.RESPONSE_MSG_SHOW_EXAM_PAPER:
                sendMessage(responseMessage);
                break;
            default:
                break;
        }
    }

    private void handleRequestMessage(RequestMessage requestMessage) {
        LOGGER.trace(JSONParser.getInstance().toJson(requestMessage));
        int messageType = requestMessage.getHeader().getMessageType();
        switch (messageType) {
            case MessageType.REQUEST_MSG_PULSE:
                rescheduleSessionTimeout();
                sendMessageToSession(requestMessage);
                break;
            case MessageType.REQUEST_MSG_START_EXAM:
                handleExamStartEvent(requestMessage);
                break;
            case MessageType.REQUEST_MSG_REGISTRATION_CONFIRMATTION:
                handleUserRegistrationConfirm(requestMessage);
                break;
            case MessageType.REQUEST_MSG_CV_UPLOAD:
                handleCvUpload(requestMessage);
                break;
            case MessageType.REQUEST_MSG_INITIATE_STUDENT_SEARCH:
                handleInitiateStudentSearch(requestMessage);
                break;
            case MessageType.REQUEST_MSG_STUDENT_SEARCH_BY_PAGINATION:
                handleStudentSearchByPagination(requestMessage);
                break;
            case MessageType.REQUEST_MSG_SHOW_EXAM_PAPER:
                handleShowExamPaper(requestMessage);
                break;
            case MessageType.REQUEST_MSG_MARK_PAPER:
            case MessageType.REQUEST_MSG_CACHING_EXAM_ANSWERS:
                handleEvent(requestMessage);
                break;
            default:
                break;
        }
    }

    private void sendToStudentSearchActor(String userId, Object messege) {
        try {
            // create search actor
            ActorRef searchActor = ActorUtils.getChildActor(getContext(), ActorName.StudentSearchActor,
                    userId);
            if (searchActor != null) {
                searchActor.tell(messege, getSelf());
            } else {
                LOGGER.error("No Student search actor found for message: {}, USER {}",
                        messege, userId);
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleShowExamPaper(RequestMessage requestMessage) {
        ShowPaper showPaper = Utils.getMessageInstance(requestMessage.getData(), ShowPaper.class);
        sendToStudentSearchActor(requestMessage.getHeader().getUserId(), showPaper);
    }

    private void handleStudentSearchByPagination(RequestMessage requestMessage) {
        try {
            StartSearch startSearch = Utils.getMessageInstance(requestMessage.getData(), StartSearch.class);
            sendToStudentSearchActor(requestMessage.getHeader().getUserId(), startSearch);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleInitiateStudentSearch(RequestMessage requestMessage) {
        try {
            // create search actor
            ActorRef searchActor = ActorUtils.getChildActor(getContext(), ActorName.StudentSearchActor,
                    requestMessage.getHeader().getUserId());
            if (searchActor == null) {
                searchActor = getContext().actorOf(Props.create(StudentSearchActor.class, sessionInfo)
                                .withDispatcher("client-dispatcher"),
                        ActorName.Client_ACTOR.toString()
                                + "-"
                                + requestMessage.getHeader().getUserId());
            }
            searchActor.tell(requestMessage, getSelf());
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleCvUpload(RequestMessage requestMessage) {
        CVChunk cvChunk = Utils.getMessageInstance(requestMessage.getData(), CVChunk.class);
        if (fileAssembler == null) {
            String fileName = requestMessage.getHeader().getSessionId();
            fileAssembler = new FileAssembler(fileName, cvChunk.getTotalBytes(), cvChunk.getChunkCount());
        }

        fileAssembler.addBytes(Base64.decodeBase64(Utils.getString(cvChunk.getByteString())), cvChunk.getIndex());
        if (fileAssembler.isFinished()) {
            boolean succuss = fileAssembler.createFile();
            ResponseMessage responseMessage = new ResponseMessage();
            ResponseHeader responseHeader = ResponseHeader.getNewInstance(requestMessage.getHeader());
            responseHeader.setMessageType(MessageType.RESPONSE_MSG_CV_UPLOAD);
            responseMessage.setHeader(responseHeader);
            responseMessage.setResObject(new CVStatus(succuss));
            sendMessage(responseMessage);
            fileAssembler = null;
        }
    }


    private void handleEvent(RequestMessage requestMessage) {
        try {
            ActorUtils.getExamsSupervisor(getContext())
                    .tell(requestMessage, getSelf());
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    private void handleUserRegistrationConfirm(RequestMessage requestMessage) {
        UserRegistration userRegistration = null;
        try {
            userRegistration = Utils.getMessageInstance(requestMessage.getData(), UserRegistration.class);

            User user = UserRegUtils.generateUserCredentials(userRegistration);
            userRegistration.setCv(
                    FileSystem.getCVFilePath(requestMessage.getHeader().getSessionId(), user.getUserName()));

            addToUserTables(userRegistration, user);
            addToExamTable(userRegistration, user);

            ContactMail contactMail = new ContactMail();
            contactMail.addToEmailList(userRegistration.getEmail());
            contactMail.setModel(MailUtils.addToRegistrationMap(userRegistration, user));
            RegistrationMailSenderSwingWorker mailSenderSwingWorker = new
                    RegistrationMailSenderSwingWorker(contactMail);
            mailSenderSwingWorker.execute();

        } catch (Exception e) {
            LOGGER.error("Error when handling User Registration Confirmation", e);
        }
    }

    public void addToUserTables(UserRegistration userRegistration, User user) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            DBHandler.getInstance().addToUserRegistration(userRegistration, user, handle);
            DBHandler.getInstance().addToUsers(user, handle);
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    public void addToExamTable(UserRegistration userRegistration, User user) {
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            DBHandler.getInstance().addToExamTable(userRegistration, user, handle);
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    private void handleExamStartEvent(RequestMessage requestMessage) {
        try {
            ExamStart examStart = Utils.getMessageInstance(requestMessage.getData(), ExamStart.class);
            ActorUtils.getExamsSupervisor(getContext())
                    .tell(examStart, getSelf());

        } catch (Exception e) {
            ClientUtils.sendErrorResponse(
                    requestMessage.getHeader(),
                    sessionInfo.getSession(),
                    100
            );
        }
    }

    private void sendMessageToSession(Object message) {
        try {
            LOGGER.trace(JSONParser.getInstance().toJson(message));
            if (sessionInfo.getSession().isOpen()) {
                sessionInfo.getSession().getRemote().sendString(JSONParser.getInstance()
                        .toJson(message));
                LOGGER.info("Response Sent");
            } else {
                LOGGER.error("Trying to write message to a closed session. Message: {}", message);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    private void handleSessionTimeOut(SessionTimeOutEvent sessionTimeOutEvent) {
        try {
            ActorUtils.getClientSupervisor(getContext())
                    .tell(new SessionDisconnectEvent(sessionInfo), getSelf());
        } catch (Exception e) {
            LOGGER.error("Error {}", e, e);
        }
    }

    private void rescheduleSessionTimeout() {
        if (sessionTimeout != null) {
            sessionTimeout.cancel();
        }
        sessionTimeout = getContext().getSystem().scheduler().scheduleOnce(
                Duration.ofSeconds(Settings.getInstance().getClientSettings().getSessionTimeout()),
                getSelf(),
                new SessionTimeOutEvent(),
                getContext().getSystem().dispatcher(),
                getSelf()
        );
    }

    private ResponseHeader getResponseHeader(String responseStoreID) {
        RequestHeader requestHeader = RequestHeaderDataStore.getSharedInstance().getData(responseStoreID);
        if (requestHeader != null) {
            return ResponseHeader.getNewInstance(requestHeader);
        } else {
            return null;
        }
    }


    private void handleSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

}
