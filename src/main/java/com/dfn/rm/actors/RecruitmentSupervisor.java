package com.dfn.rm.actors;

import akka.actor.AbstractActor;
import akka.actor.OneForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import akka.japi.pf.DeciderBuilder;
import akka.japi.pf.ReceiveBuilder;
import com.dfn.rm.util.ActorName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;

import static akka.actor.SupervisorStrategy.resume;

/**
 * This actor is responsible for supervising examsupervisor and client supervisor.
 */

public class RecruitmentSupervisor extends AbstractActor {

    private static final Logger LOGGER = LogManager.getLogger(RecruitmentSupervisor.class);
    private static final int MAX_NO_OF_RETRIES = 1;
    private static final int DURATION = 8;
    private static SupervisorStrategy strategy =
            new OneForOneStrategy(
                    MAX_NO_OF_RETRIES,
                    Duration.ofSeconds(DURATION),
                    DeciderBuilder.match(Exception.class, e -> resume())
                            .build());

    @Override
    public void preStart() throws Exception {
        super.preStart();
        LOGGER.info("Starting Recruitment Supervisor");
        getContext().actorOf(Props.create(ExamsSupervisor.class)
                        .withDispatcher("supervisor-dispatcher"),
                ActorName.EXAMS_SUPERVISOR.toString());
        getContext().actorOf(Props.create(ClientSupervisor.class)
                        .withDispatcher("supervisor-dispatcher"),
                ActorName.CLIENT_SUPERVISOR.toString());
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create().build();
    }
}
