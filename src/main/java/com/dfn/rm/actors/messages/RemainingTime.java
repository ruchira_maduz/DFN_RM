package com.dfn.rm.actors.messages;

/**
 * Message Class RemainingTime Passing across Actors .
 */

public class RemainingTime {

    private String timeRemaining;
    private boolean overTime;

    public RemainingTime() {

    }

    public RemainingTime(String timeRemaining, boolean overTime) {
        this.timeRemaining = timeRemaining;
        this.overTime = overTime;
    }

    public String getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(String timeRemaining) {
        this.timeRemaining = timeRemaining;
    }
}
