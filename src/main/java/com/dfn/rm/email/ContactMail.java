package com.dfn.rm.email;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Contact Mail.
 */

public class ContactMail {
    private String mailBody;
    private String name;
    private Map model;
    private File attachments;
    private List<String> emailList;


    public ContactMail() {
        emailList = new ArrayList<>();
    }

    public String getMailBody() {
        return mailBody;
    }

    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map getModel() {
        return model;
    }

    public void setModel(Map model) {
        this.model = model;
    }

    public File getAttachments() {
        return attachments;
    }

    public void setAttachments(File attachments) {
        this.attachments = attachments;
    }

    public void addToEmailList(String toEmailAddress) {
        emailList.add(toEmailAddress);
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public List<String> getEmailList() {
        return emailList;
    }
}
