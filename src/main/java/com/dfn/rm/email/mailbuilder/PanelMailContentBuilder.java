package com.dfn.rm.email.mailbuilder;

/**
 * panel Mail Content builder for generate panel html template .
 */

public class PanelMailContentBuilder extends MailContentBuilder {

    public PanelMailContentBuilder() {
        defaultHtmlTemplate = "templates/PanelMailTemplate.html";
    }

    public String build() {
        return super.build();
    }
}
