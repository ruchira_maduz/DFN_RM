package com.dfn.rm.email.mailbuilder;

/**
 * Registration Mail Content builder for generate registration html template .
 */

public class RegistrationMailContentBuilder extends MailContentBuilder {

    public RegistrationMailContentBuilder() {
        defaultHtmlTemplate = "templates/RegistrationMailTemplate.html";
    }

    public String build() {
        return super.build();
    }

}