package com.dfn.rm.email.mailbuilder;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


/**
 * Common Mail Content builder for generate common html template .
 */

public class MailContentBuilder {

    protected static String defaultHtmlTemplate;

    public MailContentBuilder() {
        defaultHtmlTemplate = "templates/simpleTemplate.html";
    }

    public String build() {
        InputStream inputStream = null;
        String content = "";
        try {
            inputStream = new FileInputStream(defaultHtmlTemplate);
            content = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {

        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return content;
    }
}
