package com.dfn.rm.email;


/**
 * Mail properties .
 */

public final class MailProperties {
    public static final String SMTP_HOST = "outlook.office365.com";
    public static final int PORT = 587;
    public static final String FROM_ADDRESS = "m.ruchira@directfn.com";
    public static final String USER_NAME = "m.ruchira@directfn.com";
    public static final String PASSWORD = "Maduz@123456";
    public static final boolean IS_AUTH_ENABLED = true;
    public static final boolean IS_TLS_ENABLED = true;

    private MailProperties() {

    }
}
