package com.dfn.rm.connector.fileSystem;

import com.dfn.rm.beans.Answer;
import com.dfn.rm.beans.Question;
import com.dfn.rm.util.Utils;
import com.itextpdf.text.*;

import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * paperPdf generating class.
 */

public class PaperPDF {

    private static final Logger LOGGER = LogManager.getLogger(PaperPDF.class);

    private Document document;
    private PdfWriter writer;
    private static final Font NUMBER_FONT = new Font(
            Font.FontFamily.TIMES_ROMAN, 16, Font.BOLDITALIC, BaseColor.BLACK);
    private static final Font MARKS_FONT = new Font(
            Font.FontFamily.TIMES_ROMAN, 17, Font.BOLD, BaseColor.BLACK);
    private static final Font TITLE_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 15, Font.NORMAL, BaseColor.GRAY);
    private static final Font BODY_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 15, Font.NORMAL, BaseColor.BLACK);
    private static final Font ANSWER_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 15, Font.NORMAL, BaseColor.RED);
    private float paragraphHeight;


    public PaperPDF() {

    }

    private void createMarksParagraph(double marks) {
        Paragraph marksParagraph = new Paragraph();
        marksParagraph.setFont(MARKS_FONT);
        marksParagraph.setAlignment(Paragraph.ALIGN_CENTER);
        marksParagraph.add("Marks: " + marks);

        PdfPTable table = new PdfPTable(1);
        PdfPCell cell = new PdfPCell(marksParagraph);
        cell.setBorder(PdfPCell.BOTTOM | PdfPCell.TOP | PdfPCell.LEFT | PdfPCell.RIGHT);
        cell.setBorderColor(BaseColor.LIGHT_GRAY);
        cell.setBorderWidth(2);
        cell.setPadding(5);
        cell.setPaddingTop(3);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        table.setWidthPercentage(25);
        table.setHorizontalAlignment(Element.ALIGN_RIGHT);

        try {
            document.add(table);
        } catch (DocumentException e) {
            LOGGER.error("Error When Adding Marks Paragraph to document", e);
        }

    }

    private void creatPdfBody(List<Question> questionList) {

        for (int i = 0; i < questionList.size(); i++) {
            createQuestionParagraph(i, questionList.get(i));
        }
    }


    private void createList(Paragraph paragraph, List<Answer> answerList, int type) {
        Paragraph listParagraph = new Paragraph();
        listParagraph.setIndentationLeft(25);
        com.itextpdf.text.List list = new com.itextpdf.text.List(false, true, 20);
        for (Answer answer : answerList) {
            if (!Utils.isNullOrEmptyString(answer.getText())) {
                Paragraph answerText = new Paragraph(answer.getText(), ANSWER_FONT);
                list.add(new ListItem(answerText));
                paragraphHeight += ANSWER_FONT.getSize();
            } else if (!Utils.isNullOrEmptyString(answer.getUrl())) {
                try {
                    Image image = Image.getInstance(new URL(answer.getUrl()));
                    Paragraph imageParagraph = new Paragraph();
                    imageParagraph.setFont(ANSWER_FONT);
                    imageParagraph.add(new Chunk(image, 0, 0, true));
                    list.add(new ListItem(imageParagraph));
                    paragraphHeight += image.getScaledHeight();
                } catch (BadElementException e) {
                    LOGGER.error("Error When creating List ", e);
                } catch (IOException e) {
                    LOGGER.error("Error When creating List", e);
                }
            }
        }
        listParagraph.add(list);
        paragraph.add(listParagraph);
    }

    private void createQuestionParagraph(int number, Question question) {
        paragraphHeight = 0;
        Paragraph questionParagraph = new Paragraph();

        Paragraph questionNo = new Paragraph(number + 1 + ")", NUMBER_FONT);
        questionParagraph.add(questionNo);
        paragraphHeight += NUMBER_FONT.getSize();

        if (!Utils.isNullOrEmptyString(question.getTitle())) {
            Paragraph title = new Paragraph(question.getTitle(), TITLE_FONT);
            title.setIndentationLeft(15);
            questionParagraph.add(title);
            addSpace(questionParagraph, 1);
            paragraphHeight += TITLE_FONT.getSize() + 1;
        }
        if (!Utils.isNullOrEmptyString(question.getBody())) {
            Paragraph body = new Paragraph(question.getBody(), BODY_FONT);
            body.setIndentationLeft(15);
            questionParagraph.add(body);
            addSpace(questionParagraph, 1);
            paragraphHeight += BODY_FONT.getSize() + 1;
        }
        if (!Utils.isNullOrEmptyString(question.getUrl())) {
            try {
                com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(new URL(question.getUrl()));
                image.setAlignment(Image.LEFT | com.itextpdf.text.Image.TEXTWRAP);
                Paragraph imageParagraph = new Paragraph();
                imageParagraph.add(new Chunk(image, 0, 0, true));
                imageParagraph.setIndentationLeft(15);
                questionParagraph.add(imageParagraph);
                paragraphHeight += image.getScaledHeight();
            } catch (BadElementException e) {

            } catch (IOException e) {

            }
            addSpace(questionParagraph, 1);
        }
        paragraphHeight += BODY_FONT.getSize() + 6;
        questionParagraph.setSpacingAfter(10);
        createList(questionParagraph, question.getAnswer(), question.getType());
        addSpace(questionParagraph, 10);

        try {
            if ((writer.getVerticalPosition(true) - document.bottom()) < paragraphHeight) {
                document.newPage();
            }
            document.add(questionParagraph);
        } catch (DocumentException e) {
            LOGGER.error("Error When Adding Question: " + number + " to the document", e);
        }
    }

    private static void addSpace(Paragraph paragraph, float number) {
        paragraph.setSpacingAfter(number);
    }


    public boolean createPDF(List<Question> questionList, String name, double marks) {
        boolean status = true;
        try {
            document = new Document(PageSize.A4, 36, 36, 150, 50);
            writer = PdfWriter.getInstance(document, new FileOutputStream(FileSystem.getPaperFilePathByName(name)));


            // add header and footer
            PageEvent event = new PageEvent();
            writer.setPageEvent(event);

            document.open();
            createMarksParagraph(marks);
            creatPdfBody(questionList);
            document.close();
        } catch (DocumentException e) {
            status = false;
            LOGGER.error("Error When creating Pdf", e);
        } catch (FileNotFoundException e) {
            status = false;
            LOGGER.error("Error When creating Pdf", e);
        }
        return status;
    }
}
