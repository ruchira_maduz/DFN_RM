package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.Question;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Sql data mapper for exam question data.
 */
public class ExamQuestionMapper implements RowMapper<Question> {

    @Override
    public Question map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        Question question = new Question();
        question.setId(resultSet.getLong("id"));
        question.setTitle(resultSet.getString("title"));
        question.setType(resultSet.getInt("type"));
        question.setDifficulty(resultSet.getInt("difficulty"));
        question.setUrl(resultSet.getString("image_url"));
        question.setBody(resultSet.getString("question"));
        question.setCategory(resultSet.getInt("category"));
        return question;
    }
}
