package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.User;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Sql data mapper for user data.
 */
public class UserMapper implements RowMapper<User> {

    @Override
    public User map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        User user = new User();

        user.setUserName(resultSet.getString("username"));
        user.setPasswordHash(resultSet.getString("password_hash"));
        user.setPassword(resultSet.getString("password"));
        user.setSalt(resultSet.getString("salt"));
        user.setFullName(resultSet.getString("fullname"));
        user.setUserType(resultSet.getInt("type"));

        return user;
    }
}
