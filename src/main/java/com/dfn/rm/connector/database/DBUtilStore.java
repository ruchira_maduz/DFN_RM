package com.dfn.rm.connector.database;

import com.dfn.rm.util.settings.DBSettings;
import com.dfn.rm.util.settings.Settings;
import com.mysql.cj.jdbc.MysqlDataSource;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;
import org.jdbi.v3.core.Jdbi;

import java.sql.SQLException;

/**
 * Store for managing DBI instance according to the current configuration.
 */
public final class DBUtilStore {

    private static final Logger LOGGER = LogManager.getLogger(DBUtilStore.class);
    private static final DBUtilStore INSTANCE = new DBUtilStore();

    public static DBUtilStore getInstance() {
        return INSTANCE;
    }

    private static Jdbi dbiInstance;

    private DBUtilStore() {
        DBSettings dbSettings = Settings.getInstance().getDbSettings();
        String connectionURL = "";
        JdbcConnectionPool connectionPool = null;
        switch (dbSettings.getDatabaseType().toLowerCase()) {
            case "mysql":
                // connectionURL = "jdbc:mysql://" + "127.0.0.1:3306" + "/" + "dfn_rm"+ "?user=" +
                // "root" + "&password=" + "" +
                // "&autoReconnect=true&failOverReadOnly=false&maxReconnects=10&GLOBAL max_connections = 1500";
                try {
                    MysqlDataSource mysqlDataSource = new MysqlDataSource();
                    mysqlDataSource.setServerName(dbSettings.getDatabaseURL());
                    mysqlDataSource.setPortNumber(3306);
                    mysqlDataSource.setDatabaseName(dbSettings.getDatabaseName());
                    mysqlDataSource.setUser(dbSettings.getUsername());
                    mysqlDataSource.setPassword(dbSettings.getPassword());
                    mysqlDataSource.setLoginTimeout(dbSettings.getConnectionTimeOut());

                    dbiInstance = Jdbi.create(mysqlDataSource);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "oracle":
                connectionURL = "jdbc:oracle:" + dbSettings.getDriverType() + ":@"
                        + dbSettings.getDatabaseURL() + ":" + dbSettings.getDatabaseName();
                try {
                    OracleDataSource oracleSource = new OracleDataSource();
                    oracleSource.setURL(connectionURL);
                    oracleSource.setUser(dbSettings.getUsername());
                    oracleSource.setPassword(dbSettings.getPassword());
                    oracleSource.setLoginTimeout(dbSettings.getConnectionTimeOut());
                    oracleSource.setExplicitCachingEnabled(true);
                    oracleSource.setConnectionCachingEnabled(true);
                    oracleSource.setConnectionCacheName("CACHE");
                    dbiInstance = Jdbi.create(oracleSource);
                } catch (SQLException e) {
                    LOGGER.error(e);
                    throw new RuntimeException(e);
                }
                break;
            case "h2":
                connectionPool = JdbcConnectionPool.create("jdbc:h2:mem:" + dbSettings.getDatabaseName(),
                        dbSettings.getUsername(), dbSettings.getPassword());
                connectionPool.setLoginTimeout(dbSettings.getConnectionTimeOut());
                connectionPool.setMaxConnections(dbSettings.getBatchSize());

                dbiInstance = Jdbi.create(connectionPool);
                break;
            default:
                LOGGER.debug("Can't create DB connection");
        }
    }

    public Jdbi getDbiInstance() {
        return dbiInstance;
    }
}
