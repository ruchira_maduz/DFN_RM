package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.DifficultyConfig;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DB Resultset mapper for exam difficulty information.
 */

public class ExamDifficultyMapper implements RowMapper<DifficultyConfig> {

    @Override
    public DifficultyConfig map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        DifficultyConfig difficultyConfig = new DifficultyConfig();
        difficultyConfig.setId(resultSet.getLong("id"));
        difficultyConfig.setDiffHardQuestions(resultSet.getInt("diff_hard_questions"));
        difficultyConfig.setDiffEasyQuestions(resultSet.getInt("diff_easy_questions"));
        difficultyConfig.setDiffMediumQuestions(resultSet.getInt("diff_medium_questions"));
        difficultyConfig.setIqQuestions(resultSet.getInt("iq_qs"));
        difficultyConfig.setTechQuestions(resultSet.getInt("tech_qs"));
        difficultyConfig.setTags(resultSet.getString("tags"));
        return difficultyConfig;
    }
}