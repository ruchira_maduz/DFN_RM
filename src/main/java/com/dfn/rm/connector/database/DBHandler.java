package com.dfn.rm.connector.database;

import com.dfn.rm.actors.messages.StartSearch;
import com.dfn.rm.beans.*;
import com.dfn.rm.connector.database.dao.ExamPaperDAO;
import com.dfn.rm.connector.database.dao.StudentDetailDAO;
import com.dfn.rm.connector.database.dao.UserDAO;
import com.dfn.rm.util.Constants;
import com.dfn.rm.util.DateUtils;
import com.dfn.rm.util.UserRegUtils;
import com.dfn.rm.util.Utils;
import org.jdbi.v3.core.Handle;

import java.util.List;

/**
 * DB Handler.
 */

public final class DBHandler {

    private static final DBHandler INSTANCE = new DBHandler();

    private DBUtilStore dbUtilStore = DBUtilStore.getInstance();

    private DBHandler() {

    }

    public static DBHandler getInstance() {
        return INSTANCE;
    }

    public User getUser(String username, Handle handle) {
        UserDAO userDAO = handle.attach(UserDAO.class);
        return userDAO.getUser(username);
    }

    public UserRegistration getUserRegistration(String userName, Handle handle) {
        UserDAO userDAO = handle.attach(UserDAO.class);
        return userDAO.getUserRegistration(userName);
    }

    public List<ExamDetails> getExamDetails(String username, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getExamDetails(username);
    }

    public DifficultyConfig getDifficultyConfig(String position, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getDifficultyConfig(position);
    }

    public List<Question> getQuestionsFromCategory(int category, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getQuestionFromQuestionCategory(category);
    }

    public List<Question> getQuestions(Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getQuestions();
    }

    public Question getQuestion(int questionID, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getQuestion(questionID);
    }

    public List<Answer> getAnswersOfQuestion(long questionID, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getAnswers(questionID);
    }

    public List<Answer> getAllAnswers(Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getAllAnswers();
    }

    public List<Answer> getAllAnswersWithWeight(Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getAllAnswersWithWeight();
    }

    public List<ExamAnswer> getMarkedAnswers(String userID, String paperID, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getMarkedQuestions(userID, paperID);
    }

    public ExamAnswer getQuestionByID(String userId, String paperId, int questionId, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getQuestionsByID(userId, paperId, questionId);
    }

    public ExamActorRecovery getExamActorSnapShot(long examID, String userID, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        return paperDAO.getExamActorSnapShot(userID, examID);
    }

    public void addToExamPaperRecovery(ExamActorRecovery examActorRecovery, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        paperDAO.addtoExamPaperRecovery(
                examActorRecovery.getUserID(),
                examActorRecovery.getPaperID(),
                examActorRecovery.getExamID(),
                examActorRecovery.getElapsedTime(),
                examActorRecovery.getOverlyElapsed(),
                examActorRecovery.getPaperRecoveryByte());
    }

    public void updateExamActorRecovery(long elapsedTime, String userID, long examID, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        paperDAO.updateExamActorRecovery(elapsedTime, userID, examID);
    }

    public void updateExamActorRecoverySecondary(long elapsedTime, int overlyElapsed, String userID,
                                                 long examID, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        paperDAO.updateExamActorRecoverySecondary(elapsedTime, overlyElapsed, userID, examID);
    }

    public void addToExamAnswer(ExamAnswer examAnswer, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        paperDAO.addToExamAnswer(
                examAnswer.getUserID(),
                examAnswer.getPaperID(),
                examAnswer.getQuestionID(),
                examAnswer.getAnswerID()
        );
    }

    public void updateExamAnswer(ExamAnswer examAnswer, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        paperDAO.updateExamAnswer(
                examAnswer.getUserID(),
                examAnswer.getPaperID(),
                examAnswer.getQuestionID(),
                examAnswer.getAnswerID()
        );
    }

    public void deleteFromExamAnswer(ExamAnswer examAnswer, Handle handle) {
        ExamPaperDAO paperDAO = handle.attach(ExamPaperDAO.class);
        paperDAO.deleteFromExamAnswer(
                examAnswer.getUserID(),
                examAnswer.getPaperID(),
                examAnswer.getQuestionID(),
                examAnswer.getAnswerID()
        );
    }

    public void addToExamTable(UserRegistration userRegistration, User user, Handle handle) {
        ExamPaperDAO examPaperDAO = handle.attach(ExamPaperDAO.class);
        examPaperDAO.addToExams(
                user.getUserName(),
                UserRegUtils.generatePaperID(user.getUserName()),
                Constants.DURATION,
                userRegistration.getPosition(),
                Constants.EXAM_ON_HOLD,
                Constants.AUTO_GENERATE,
                Constants.IQ_QUESTIONS_COUNT,
                Constants.EXAM_CATEGORY_IQ);
    }


    public void addToUserRegistration(UserRegistration userRegistration, User user, Handle handle) {
        UserDAO userDAO = handle.attach(UserDAO.class);
        userDAO.addUser(
                userRegistration.getNic(),
                userRegistration.getName(),
                user.getUserName(),
                userRegistration.getDepartment(),
                userRegistration.getEmail(),
                userRegistration.getMobile(),
                userRegistration.getUniveristy(),
                userRegistration.getPosition(),
                userRegistration.getCv(),
                userRegistration.getTechIntviewDate(),
                userRegistration.getTechIntviewTime(),
                userRegistration.getTechIntviewPanel());
    }

    public void addToUsers(User user, Handle handle) {
        UserDAO userDAO = handle.attach(UserDAO.class);
        userDAO.addUserToUserTable(
                user.getUserName(),
                user.getPasswordHash(),
                user.getPassword(),
                user.getSalt(),
                user.getUserType(),
                user.getFullName());
    }

    public void removeFromUserRegistration(String nic, Handle handle) {
        UserDAO userDAO = handle.attach(UserDAO.class);
        userDAO.deleteUser(nic);
    }

    public void updateExams(String userID, int status, double marks, String paper, Handle handle) {
        ExamPaperDAO examPaperDAO = handle.attach(ExamPaperDAO.class);
        examPaperDAO.updateExams(status, marks, paper, userID);
    }

    public List<StudentSearchRecord> searchByPagination(StartSearch startSearch, Handle handle) {
        StudentDetailDAO studentDetailDAO = handle.attach(StudentDetailDAO.class);
        return studentDetailDAO.searchByPagination(
                Utils.formatField(startSearch.getNic()),
                Utils.formatField(startSearch.getName()),
                Utils.formatField(startSearch.getDepartment()),
                Utils.formatField(startSearch.getMarks()),
                Utils.formatField(startSearch.getFromDate()),
                DateUtils.formatDateField(startSearch.getFromDate(), startSearch.getToDate()),
                Utils.formatField(startSearch.getUniversity()),
                Utils.formatField(startSearch.getPosition()),
                Constants.SEARCH_COUNT_PER_PAGE,
                Utils.getPageNo(startSearch.getPageNo())
        );
    }

    public List<StudentSearchRecord> studentSearch(StartSearch startSearch, Handle handle) {
        StudentDetailDAO studentDetailDAO = handle.attach(StudentDetailDAO.class);
        return studentDetailDAO.studentSearch(
                Utils.formatField(startSearch.getNic()),
                Utils.formatField(startSearch.getName()),
                Utils.formatField(startSearch.getDepartment()),
                Utils.formatField(startSearch.getMarks()),
                Utils.formatField(startSearch.getFromDate()),
                DateUtils.formatDateField(startSearch.getFromDate(), startSearch.getToDate()),
                Utils.formatField(startSearch.getUniversity()),
                Utils.formatField(startSearch.getPosition())
        );
    }
}
