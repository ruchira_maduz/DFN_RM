package com.dfn.rm.connector.database.dao;

import com.dfn.rm.beans.StudentSearchRecord;
import com.dfn.rm.connector.database.mapper.StudentSearchMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;


/**
 * Data Access Object for Creating Tables of DB.
 */


public interface StudentDetailDAO {

    @SqlQuery("SELECT USER_REGISTRATION.* ,EXAMS.MARKS ,EXAMS.PAPER FROM USER_REGISTRATION, EXAMS  WHERE "
            + "USER_REGISTRATION.USER_ID = EXAMS.USER_ID AND STATUS = 2 "
            + " AND ((:NIC IS NULL) OR (NIC = :NIC)) "
            + " AND ((:NAME IS NULL) OR (UPPER(NAME) LIKE CONCAT(:NAME,'%'))) "
            + " AND ((:DEPARTMENT IS NULL) OR (UPPER(DEPARTMENT) LIKE CONCAT(:DEPARTMENT,'%'))) "
            + " AND ((:MARKS IS NULL) OR (MARKS = :MARKS)) "
            + " AND ((:TODATE IS NULL) OR (TECH_INTERVIEW_DATE <= :TODATE)) "
            + " AND ((:FROMDATE IS NULL or :TODATE IS NULL) or (TECH_INTERVIEW_DATE between :FROMDATE AND :TODATE)) "
            + " AND ((:UNIVERSITY IS NULL) OR (UPPER(UNIVERSITY) LIKE CONCAT(:UNIVERSITY,'%'))) "
            + " AND ((:POSITION IS NULL) OR (UPPER(USER_REGISTRATION.POSITION) LIKE CONCAT(:POSITION,'%')))"
            + " LIMIT :PAGECOUNT OFFSET :PAGINATION")
    @UseRowMapper(StudentSearchMapper.class)
    List<StudentSearchRecord> searchByPagination(@Bind("NIC") String nic,
                                                 @Bind("NAME") String name,
                                                 @Bind("DEPARTMENT") String department,
                                                 @Bind("MARKS") String marks,
                                                 @Bind("FROMDATE") String fromDate,
                                                 @Bind("TODATE") String toDate,
                                                 @Bind("UNIVERSITY") String uni,
                                                 @Bind("POSITION") String position,
                                                 @Bind("PAGECOUNT") int pageCount,
                                                 @Bind("PAGINATION") int pageNo);

    @SqlQuery("SELECT USER_REGISTRATION.* ,EXAMS.MARKS ,EXAMS.PAPER FROM USER_REGISTRATION, EXAMS  WHERE "
            + "USER_REGISTRATION.USER_ID = EXAMS.USER_ID AND STATUS = 2 "
            + " AND ((:NIC IS NULL) OR (NIC = :NIC)) "
            + " AND ((:NAME IS NULL) OR (UPPER(NAME) LIKE CONCAT(:NAME,'%'))) "
            + " AND ((:DEPARTMENT IS NULL) OR (UPPER(DEPARTMENT) LIKE CONCAT(:DEPARTMENT,'%'))) "
            + " AND ((:MARKS IS NULL) OR (MARKS = :MARKS)) "
            + " AND ((:TODATE IS NULL) OR (TECH_INTERVIEW_DATE <= :TODATE)) "
            + " AND ((:FROMDATE IS NULL or :TODATE IS NULL) or (TECH_INTERVIEW_DATE between :FROMDATE AND :TODATE)) "
            + " AND ((:UNIVERSITY IS NULL) OR (UPPER(UNIVERSITY) LIKE CONCAT(:UNIVERSITY,'%'))) "
            + " AND ((:POSITION IS NULL) OR (UPPER(USER_REGISTRATION.POSITION) LIKE CONCAT(:POSITION,'%')))")
    @UseRowMapper(StudentSearchMapper.class)
    List<StudentSearchRecord> studentSearch(@Bind("NIC") String nic,
                                            @Bind("NAME") String name,
                                            @Bind("DEPARTMENT") String department,
                                            @Bind("MARKS") String marks,
                                            @Bind("FROMDATE") String fromDate,
                                            @Bind("TODATE") String toDate,
                                            @Bind("UNIVERSITY") String uni,
                                            @Bind("POSITION") String position);
}
