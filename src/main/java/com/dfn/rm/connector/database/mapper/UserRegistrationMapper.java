package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.UserRegistration;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Sql data mapper for user registration data.
 */

public class UserRegistrationMapper implements RowMapper<UserRegistration> {

    @Override
    public UserRegistration map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        UserRegistration userRegistration = new UserRegistration();

        userRegistration.setId(resultSet.getLong("ID"));
        userRegistration.setNic(resultSet.getString("NIC"));
        userRegistration.setName(resultSet.getString("NAME"));
        userRegistration.setDepartment(resultSet.getString("DEPARTMENT"));
        userRegistration.setEmail(resultSet.getString("EMAIL"));
        userRegistration.setMobile(resultSet.getString("MOBILE"));
        userRegistration.setUniveristy(resultSet.getString("UNIVERSITY"));
        userRegistration.setPosition(resultSet.getString("POSITION"));
        userRegistration.setCv(resultSet.getString("CV"));
        userRegistration.setTechIntviewDate(resultSet.getString("TECH_INTERVIEW_DATE"));
        userRegistration.setTechIntvietPanel(resultSet.getString("TECH_INTERVIEW_PANEL"));
        userRegistration.setTechIntviewTime(resultSet.getString("TECH_INTERVIEW_TIME"));

        return userRegistration;
    }
}
