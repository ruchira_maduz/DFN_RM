package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.Answer;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Sql data mapper for exam answer data.
 */
public class ExamsAnswerMapper implements RowMapper<Answer> {

    @Override
    public Answer map(ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        Answer answer = new Answer();

        answer.setId(resultSet.getInt("id"));
        answer.setType(resultSet.getInt("type"));
        answer.setText(resultSet.getString("answer"));
        answer.setUrl(resultSet.getString("image"));
        answer.setQuestionId(resultSet.getInt("question_id"));
        return answer;
    }
}
