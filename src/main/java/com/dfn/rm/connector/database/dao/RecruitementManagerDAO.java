package com.dfn.rm.connector.database.dao;

import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 * Data Access Object for Creating Tables of DB.
 */

public interface RecruitementManagerDAO {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS USERTABLE(\n"
            + "USERNAME VARCHAR(25) PRIMARY KEY,\n "
            + "PASSWORD_HASH VARCHAR(300), \n"
            + "PASSWORD VARCHAR(50), \n"
            + "SALT VARCHAR(45), TYPE INT, \n"
            + "FULLNAME VARCHAR(200))")
    void createUserTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS EXAM_ACTOR_RECOVERY(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "USER_ID VARCHAR(25) NOT NULL,\n "
            + "PAPER_ID VARCHAR(25) DEFAULT NULL, \n"
            + "EXAM_ID int(11) DEFAULT NULL, \n"
            + "ELAPSED_TIME int(11) DEFAULT NULL, \n"
            + "OVERLY_ELAPSED int(11) DEFAULT NULL, \n"
            + "PRIMARY KEY (ID),\n"
            + "PAPER BLOB)")
    void createExamPaperRecoveryTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS ANSWERS(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "QUESTION_ID INT(11) DEFAULT NULL,\n"
            + "ANSWER VARCHAR(1000) DEFAULT NULL,\n"
            + "IMAGE VARCHAR(200) DEFAULT NULL,\n"
            + "WEIGHT DOUBLE DEFAULT NULL,\n"
            + "TYPE INT(11) NOT NULL,\n"
            + "PRIMARY KEY (ID),\n"
            + "UNIQUE KEY ID_UNIQUE (ID))")
    void createAnswersTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS DIFFICULTY_CONFIG ("
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "POSITION VARCHAR(45) DEFAULT NULL,\n"
            + "IQ_QUESTIONS INT(11) DEFAULT NULL,\n"
            + "TECHNICAL_QUESTIONS INT(11) DEFAULT NULL,\n"
            + "DIFF_HARD_QUESTIONS INT(11) DEFAULT NULL,\n"
            + "DIFF_MEDIUM_QUESTIONS INT(11) DEFAULT NULL,\n"
            + "DIFFF_EASY_QUESTIONS INT(11) DEFAULT NULL,\n"
            + "TAGS VARCHAR(200) DEFAULT NULL,\n"
            + "PRIMARY KEY (id))")
    void createDiffiConfigTable();


    @SqlUpdate("CREATE TABLE IF NOT EXISTS EXAM_ANSWER (\n"
            + "  ID int(11) NOT NULL AUTO_INCREMENT,\n"
            + "  USER_ID varchar(25) DEFAULT NULL,\n"
            + "  PAPER_ID VARCHAR(25) DEFAULT NULL,\n"
            + "  QUESTION_ID int(11) DEFAULT NULL,\n"
            + "  ANSWER VARCHAR(25) DEFAULT NULL,\n"
            + "  PRIMARY KEY (ID))")
    void createExamAnswerTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS EXAM_PAPER (\n"
            + "  ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "  PAPER VARCHAR(45) NOT NULL,\n"
            + "  QUESTION_ID INT(11) NOT NULL,\n"
            + "  WEIGHT INT(11) NOT NULL DEFAULT '1',\n"
            + "  PRIMARY KEY (ID),\n"
            + "  UNIQUE KEY ID_UNIQUE (ID))")
    void createExamPaperTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS EXAMS (\n"
            + "  EXAM_ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "  USER_ID VARCHAR(200) DEFAULT NULL,\n"
            + "  PAPER_ID VARCHAR(45) DEFAULT NULL,\n"
            + "  DURATION INT(11) DEFAULT NULL,\n"
            + "  POSITION VARCHAR(45) DEFAULT NULL,\n"
            + "  MARKS DOUBLE DEFAULT NULL,\n"
            + "  STATUS INT(11) DEFAULT 0,\n"
            + "  REMARKS VARCHAR(500) DEFAULT NULL,\n"
            + "  EXPIRE_TIME INT(11) DEFAULT NULL,\n"
            + "  EXAM_TYPE INT(11) DEFAULT NULL,\n"
            + "  PAPER VARCHAR(100) DEFAULT NULL,\n"
            + "  EXAM_CATEGORY INT(11) DEFAULT NULL,\n"
            + "  NO_QUESTIONS INT(11) DEFAULT NULL,\n"
            + "  PRIMARY KEY (EXAM_ID))")
    void createExamsTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS QUESTIONS (\n"
            + "  ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "  TITLE VARCHAR(100) NOT NULL,\n"
            + "  QUESTION VARCHAR(2000) DEFAULT NULL,\n"
            + "  IMAGE_URL VARCHAR(500) DEFAULT NULL,\n"
            + "  TYPE INT(11) NOT NULL,\n"
            + "  CATEGORY INT(11) NOT NULL,\n"
            + "  DIFFICULTY INT(11) NOT NULL,\n"
            + "  PRIMARY KEY (ID),\n"
            + "  UNIQUE KEY ID_UNIQUE (ID))")
    void createQuestionsTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS USER_REGISTRATION (\n"
            + "  ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "  NIC VARCHAR(20) NOT NULL,\n"
            + "  NAME VARCHAR(500) DEFAULT NULL,\n"
            + "  USER_ID VARCHAR(20) NOT NULL,\n"
            + "  DEPARTMENT VARCHAR(500) DEFAULT NULL,\n"
            + "  EMAIL VARCHAR(200) NOT NULL,\n"
            + "  MOBILE VARCHAR(50) NOT NULL,\n"
            + "  UNIVERSITY VARCHAR(500) DEFAULT NULL,\n"
            + "  POSITION VARCHAR(500) DEFAULT NULL,\n"
            + "  CV VARCHAR(4000) DEFAULT NULL,\n"
            + "  TECH_INTERVIEW_DATE VARCHAR(100) DEFAULT NULL,\n"
            + "  TECH_INTERVIEW_TIME VARCHAR(100) DEFAULT NULL,\n"
            + "  TECH_INTERVIEW_PANEL VARCHAR(1000) DEFAULT NULL,\n"
            + "  PRIMARY KEY (ID))")
    void createUserRegistrationTable();
}
