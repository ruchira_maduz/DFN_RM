package com.dfn.rm.connector.database.mapper;

import com.dfn.rm.beans.ExamActorRecovery;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DB Resultset mapper for exam actor account state recovery  information.
 */

public class ExamActorRecoveryMapper implements RowMapper<ExamActorRecovery> {

    @Override
    public ExamActorRecovery map(ResultSet resultSet, StatementContext statementContext) throws SQLException {
        ExamActorRecovery examActorRecovery = new ExamActorRecovery();
        examActorRecovery.setId(resultSet.getInt("ID"));
        examActorRecovery.setExamID(resultSet.getLong("EXAM_ID"));
        examActorRecovery.setUserID(resultSet.getString("USER_ID"));
        examActorRecovery.setPaperID(resultSet.getString("PAPER_ID"));
        examActorRecovery.setElapsedTime(resultSet.getLong("ELAPSED_TIME"));
        examActorRecovery.setOverlyElapsed(resultSet.getInt("OVERLY_ELAPSED"));
        examActorRecovery.setPaperRecoveryByte(resultSet.getBytes("PAPER"));
        return examActorRecovery;
    }
}
