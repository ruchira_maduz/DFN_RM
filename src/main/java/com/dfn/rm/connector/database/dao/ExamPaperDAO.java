package com.dfn.rm.connector.database.dao;

import com.dfn.rm.beans.*;
import com.dfn.rm.connector.database.mapper.*;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;

/**
 * Data Access Object for Exam Paper related entities.
 */

public interface ExamPaperDAO {

    @SqlQuery("SELECT * FROM QUESTIONS WHERE ID = :QUESTION_ID")
    @UseRowMapper(ExamQuestionMapper.class)
    Question getQuestion(@Bind("QUESTION_ID") int questionId);

    @SqlQuery("SELECT * FROM QUESTIONS, EXAM_PAPER WHERE QUESTIONS.ID = EXAM_PAPER.QUESTION_ID AND PAPER_ID = "
            + ":PAPER_ID")
    @UseRowMapper(ExamQuestionMapper.class)
    List<Question> getQuestionFromPaperId(@Bind("PAPER_ID") int paperId);

    @SqlQuery("SELECT * FROM ANSWERS WHERE QUESTION_ID = :QUESTION_ID")
    @UseRowMapper(ExamsAnswerMapper.class)
    List<Answer> getAnswers(@Bind("QUESTION_ID") long questionId);

    @SqlQuery("SELECT * FROM ANSWERS")
    @UseRowMapper(ExamsAnswerMapper.class)
    List<Answer> getAllAnswers();

    @SqlQuery("SELECT * FROM ANSWERS")
    @UseRowMapper(ExamAnswersWeightMapper.class)
    List<Answer> getAllAnswersWithWeight();

    @SqlQuery("SELECT * FROM EXAMS WHERE USER_ID = :USER_ID")
    @UseRowMapper(ExamDetailsMapper.class)
    List<ExamDetails> getExamDetails(@Bind("USER_ID") String userId);

    @SqlQuery("SELECT * FROM DIFFICULTYCONFIG WHERE POSITION = :POSITION")
    @UseRowMapper(ExamDifficultyMapper.class)
    DifficultyConfig getDifficultyConfig(@Bind("POSITION") String position);

    @SqlQuery("SELECT * FROM QUESTIONS WHERE CATEGORY = :CATEGORY")
    @UseRowMapper(ExamQuestionMapper.class)
    List<Question> getQuestionFromQuestionCategory(@Bind("CATEGORY") int category);

    @SqlQuery("SELECT * FROM QUESTIONS")
    @UseRowMapper(ExamQuestionMapper.class)
    List<Question> getQuestions();

    @SqlQuery("SELECT * FROM EXAM_ACTOR_RECOVERY WHERE USER_ID = :USER_ID AND EXAM_ID = :EXAM_ID")
    @UseRowMapper(ExamActorRecoveryMapper.class)
    ExamActorRecovery getExamActorSnapShot(@Bind("USER_ID") String userID, @Bind("EXAM_ID") long examID);

    @SqlQuery("SELECT * FROM EXAM_ANSWER WHERE USER_ID = :USER_ID AND PAPER_ID = :PAPER_ID")
    @UseRowMapper(MarkedAnswersMapper.class)
    List<ExamAnswer> getMarkedQuestions(@Bind("USER_ID") String userID, @Bind("PAPER_ID") String paperID);

    @SqlQuery("SELECT * FROM EXAM_ANSWER WHERE USER_ID = :USER_ID "
            + "AND PAPER_ID = :PAPER_ID AND QUESTION_ID = :QUESTION_ID")
    @UseRowMapper(MarkedAnswersMapper.class)
    ExamAnswer getQuestionsByID(@Bind("USER_ID") String userId,
                                @Bind("PAPER_ID") String paperId,
                                @Bind("QUESTION_ID") int questionId);


    @SqlUpdate("INSERT INTO EXAM_ACTOR_RECOVERY (USER_ID, PAPER_ID, EXAM_ID, ELAPSED_TIME, OVERLY_ELAPSED, PAPER)\n"
            + "VALUES (:USER_ID, :PAPER_ID, :EXAM_ID, :ELAPSED_TIME, :OVERLY_ELAPSED, :PAPER);")
    void addtoExamPaperRecovery(@Bind("USER_ID") String userID,
                                @Bind("PAPER_ID") String paperID,
                                @Bind("EXAM_ID") long examId,
                                @Bind("ELAPSED_TIME") long elapsedTime,
                                @Bind("OVERLY_ELAPSED") int overlyElapsed,
                                @Bind("PAPER") byte[] paper);

    @SqlUpdate("UPDATE EXAM_ACTOR_RECOVERY SET ELAPSED_TIME = :ELAPSED_TIME "
            + "WHERE USER_ID = :USER_ID AND EXAM_ID = :EXAM_ID")
    void updateExamActorRecovery(@Bind("ELAPSED_TIME") long elapsedTime,
                                 @Bind("USER_ID") String userId,
                                 @Bind("EXAM_ID") long examId);

    @SqlUpdate("UPDATE EXAM_ACTOR_RECOVERY SET ELAPSED_TIME = :ELAPSED_TIME , OVERLY_ELAPSED = :OVERLY_ELAPSED  "
            + "WHERE USER_ID = :USER_ID AND EXAM_ID = :EXAM_ID")
    void updateExamActorRecoverySecondary(@Bind("ELAPSED_TIME") long elapsedTime,
                                          @Bind("OVERLY_ELAPSED") int overlyElapsed,
                                          @Bind("USER_ID") String userId,
                                          @Bind("EXAM_ID") long examId);


    @SqlUpdate("INSERT INTO EXAMS (USER_ID, PAPER_ID, DURATION, POSITION, STATUS,"
            + " EXAM_TYPE, NO_QUESTIONS, EXAM_CATEGORY)\n"
            + "VALUES (:USER_ID, :PAPER_ID, :DURATION, :POSITION, :STATUS, :EXAM_TYPE, :NO_QUESTIONS, :EXAM_CATEGORY);")
    void addToExams(@Bind("USER_ID") String userId,
                    @Bind("PAPER_ID") String paperId,
                    @Bind("DURATION") int duration,
                    @Bind("POSITION") String position,
                    @Bind("STATUS") int status,
                    @Bind("EXAM_TYPE") int examType,
                    @Bind("NO_QUESTIONS") int noQuestions,
                    @Bind("EXAM_CATEGORY") int examCategory);


    @SqlUpdate("UPDATE EXAMS SET STATUS = :STATUS , MARKS = :MARKS , PAPER = :PAPER  "
            + "WHERE USER_ID = :USER_ID")
    void updateExams(@Bind("STATUS") int status,
                     @Bind("MARKS") double marks,
                     @Bind("PAPER") String paper,
                     @Bind("USER_ID") String userId);


    @SqlUpdate("INSERT INTO EXAM_ANSWER (USER_ID, PAPER_ID, QUESTION_ID, ANSWER)\n"
            + "VALUES (:USER_ID, :PAPER_ID, :QUESTION_ID, :ANSWER);")
    void addToExamAnswer(@Bind("USER_ID") String userId,
                         @Bind("PAPER_ID") String paperId,
                         @Bind("QUESTION_ID") int questionId,
                         @Bind("ANSWER") String answerId);

    @SqlUpdate("DELETE FROM EXAM_ANSWER WHERE USER_ID = :USER_ID AND PAPER_ID = :PAPER_ID AND"
            + " QUESTION_ID = :QUESTION_ID AND ANSWER = :ANSWER ")
    void deleteFromExamAnswer(
            @Bind("USER_ID") String userId,
            @Bind("PAPER_ID") String paperId,
            @Bind("QUESTION_ID") int questionId,
            @Bind("ANSWER") String answerId);

    @SqlUpdate("UPDATE EXAM_ANSWER SET ANSWER = :ANSWER "
            + "WHERE USER_ID = :USER_ID AND PAPER_ID = :PAPER_ID AND QUESTION_ID = :QUESTION_ID")
    void updateExamAnswer(@Bind("USER_ID") String userId,
                          @Bind("PAPER_ID") String paperId,
                          @Bind("QUESTION_ID") int questionId,
                          @Bind("ANSWER") String answerId);
}
