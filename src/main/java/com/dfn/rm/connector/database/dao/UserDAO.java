package com.dfn.rm.connector.database.dao;

import com.dfn.rm.beans.User;
import com.dfn.rm.beans.UserRegistration;
import com.dfn.rm.connector.database.mapper.UserMapper;
import com.dfn.rm.connector.database.mapper.UserRegistrationMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

/**
 * Data Access Object for  User related entities.
 */

public interface UserDAO {

    @SqlQuery("SELECT * FROM USERTABLE WHERE USERNAME = :USERNAME")
    @UseRowMapper(UserMapper.class)
    User getUser(@Bind("USERNAME") String userName);

    @SqlQuery("SELECT * FROM USER_REGISTRATION WHERE USER_ID = :USER_ID")
    @UseRowMapper(UserRegistrationMapper.class)
    UserRegistration getUserRegistration(@Bind("USER_ID") String userId);

    @SqlUpdate("INSERT INTO USERTABLE (USERNAME, PASSWORD_HASH, PASSWORD,SALT, TYPE, FULLNAME)\n"
            + "VALUES (:USERNAME, :PASSWORD_HASH, :PASSWORD, :SALT, :TYPE, :FULLNAME);")
    void addUserToUserTable(@Bind("USERNAME") String username,
                            @Bind("PASSWORD_HASH") String passwordHash,
                            @Bind("PASSWORD") String password,
                            @Bind("SALT") String salt,
                            @Bind("TYPE") int type,
                            @Bind("FULLNAME") String fullname);

    @SqlUpdate("INSERT INTO USER_REGISTRATION (NIC, NAME, USER_ID, DEPARTMENT, EMAIL, MOBILE, "
            + "UNIVERSITY, POSITION, CV, TECH_INTERVIEW_DATE, TECH_INTERVIEW_TIME, TECH_INTERVIEW_PANEL)\n"
            + "VALUES (:NIC, :NAME, :USER_ID, :DEPARTMENT, :EMAIL, :MOBILE, :UNIVERSITY, :POSITION, "
            + ":CV, :TECH_INTERVIEW_DATE, :TECH_INTERVIEW_TIME, :TECH_INTERVIEW_PANEL);")
    void addUser(@Bind("NIC") String nic,
                 @Bind("NAME") String name,
                 @Bind("USER_ID") String userId,
                 @Bind("DEPARTMENT") String department,
                 @Bind("EMAIL") String email,
                 @Bind("MOBILE") String mobile,
                 @Bind("UNIVERSITY") String university,
                 @Bind("POSITION") String position,
                 @Bind("CV") String cv,
                 @Bind("TECH_INTERVIEW_DATE") String techInterviewDate,
                 @Bind("TECH_INTERVIEW_TIME") String techInterviewTime,
                 @Bind("TECH_INTERVIEW_PANEL") String techInterviewPanel);


    @SqlUpdate("UPDATE USER_REGISTRATION\n"
            + "SET CV = :CV WHERE NIC = :NIC;")
    void setUserRegistration(@Bind("USER_ID") String userId,
                             @Bind("CV") String cv);

    @SqlUpdate("DELETE FROM USER_REGISTRATION\n"
            + "WHERE NIC = NIC;")
    void deleteUser(@Bind("NIC") String nic);
}
