package com.dfn.rm.connector.websocket.messages;

import com.google.gson.annotations.SerializedName;

/**
 * Response message for a heartbeat pulse.
 */
public class PulseResponse {

    @SerializedName("HED")
    final RequestHeader requestHeader;

    public PulseResponse() {
        this.requestHeader = new RequestHeader();
        this.requestHeader.setMessageType(0);
    }
}
