package com.dfn.rm.connector.websocket.events;

import org.eclipse.jetty.websocket.api.Session;

/**
 * Event for indicating the closing of a session.
 */
public class SessionCloseEvent {
    private final Session session;

    public SessionCloseEvent(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }
}
