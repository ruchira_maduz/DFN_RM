package com.dfn.rm.connector.websocket.messages;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manodyas on 9/8/2017.
 */
public class ResponseMessage {
    @SerializedName("HED")
    private ResponseHeader header;
    @SerializedName("DAT")
    private Object resObject = null;

    public ResponseHeader getHeader() {
        return header;
    }

    public void setHeader(ResponseHeader header) {
        this.header = header;
    }

    public Object getResObject() {
        return resObject;
    }

    public void setResObject(Object resObject) {
        this.resObject = resObject;
    }
}
