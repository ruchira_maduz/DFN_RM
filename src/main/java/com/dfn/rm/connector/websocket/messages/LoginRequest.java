package com.dfn.rm.connector.websocket.messages;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manodyas on 9/8/2017.
 */
public class LoginRequest {
    @SerializedName("lgnNme")
    private String usrname;
    @SerializedName("pwd")
    private String password;

    public String getUsrname() {
        return usrname;
    }

    public void setUsrname(String usrname) {
        this.usrname = usrname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
