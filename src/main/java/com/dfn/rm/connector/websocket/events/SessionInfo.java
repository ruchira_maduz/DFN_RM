package com.dfn.rm.connector.websocket.events;

import org.eclipse.jetty.websocket.api.Session;

/**
 * Session information particular for a client session.
 */
public class SessionInfo {
    private final String sessionID;
    private Session session;
    private boolean isAuthenticated = false;
    private String userID;
    private String clientIP;
    private int userType;
    private String tenantCode;
    private String commVersion;

    public SessionInfo(String sessionID, Session session) {
        this.sessionID = sessionID;
        this.setSession(session);
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        isAuthenticated = authenticated;
    }

    public Session getSession() {
        return session;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public String getSessionID() {
        return sessionID;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getCommVersion() {
        return commVersion;
    }

    public void setCommVersion(String commVersion) {
        this.commVersion = commVersion;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
