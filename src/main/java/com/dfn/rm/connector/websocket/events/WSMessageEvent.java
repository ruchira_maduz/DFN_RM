package com.dfn.rm.connector.websocket.events;

import org.eclipse.jetty.websocket.api.Session;

/**
 * Web Socket message event.
 */
public class WSMessageEvent {
    private final String message;
    private final Session session;

    public WSMessageEvent(String message, Session session) {
        this.message = message;
        this.session = session;
    }

    public String getMessage() {
        return message;
    }

    public Session getSession() {
        return session;
    }
}
