package com.dfn.rm.connector.websocket.messages;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manodyas on 9/8/2017.
 */
public class RequestHeader {
    @SerializedName("msgTyp")
    private Integer messageType;
    @SerializedName("channel")
    private Integer channelId;
    @SerializedName("commVer")
    private String clientVersion;
    @SerializedName("loginId")
    private String userId;
    @SerializedName("sesnId")
    private String sessionId;
    @SerializedName("clientIp")
    private String clientIp;
    @SerializedName("tenantCode")
    private String tenantCode;
    @SerializedName("unqReqId")
    private String unqReqId;

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getUnqReqId() {
        return unqReqId;
    }

    public void setUnqReqId(String unqReqId) {
        this.unqReqId = unqReqId;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    @Override
    public String toString() {
        return "RequestHeader{"
                + "messageType=" + messageType
                + ", channelId=" + channelId
                + ", clientVersion='" + clientVersion + '\''
                + ", userId='" + userId + '\''
                + ", sessionId='" + sessionId + '\''
                + ", clientIp='" + clientIp + '\''
                + ", tenantCode='" + tenantCode + '\''
                + ", unqReqId='" + unqReqId + '\''
                + '}';
    }
}
