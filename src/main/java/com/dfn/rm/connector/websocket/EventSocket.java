package com.dfn.rm.connector.websocket;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.dfn.rm.connector.websocket.events.SessionCloseEvent;
import com.dfn.rm.connector.websocket.events.SessionConnectEvent;
import com.dfn.rm.connector.websocket.events.WSMessageEvent;
import com.dfn.rm.util.ActorUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;


/**
 * WebSocket adapter for reading the messages sent through websocket connection.
 */
public class EventSocket extends WebSocketAdapter {

    private static final Logger LOGGER = LogManager.getLogger(EventSocket.class);

    private static AbstractActor.ActorContext context;

    public static void setClientSupervisor(AbstractActor.ActorContext clientSupervisor) {
        EventSocket.context = clientSupervisor;
    }

    @Override
    public void onWebSocketConnect(Session session) {
        super.onWebSocketConnect(session);
        LOGGER.debug("WS Connected Session: {}", session);
        try {
            ActorUtils.getClientSupervisor(context).tell(new SessionConnectEvent(session), ActorRef.noSender());
        } catch (Exception ex) {
            LOGGER.error("Error occurred during connecting the session. {}, Session: {}", ex, session);
        }

    }

    @Override
    public boolean isConnected() {
        LOGGER.debug("WS Connected...");
        return super.isConnected();
    }

    @Override
    public void onWebSocketText(String message) {
        super.onWebSocketText(message);
        try {
            ActorUtils.getClientSupervisor(context).tell(new WSMessageEvent(message, this.getSession()),
                                                         ActorRef.noSender());
        } catch (Exception ex) {
            LOGGER.error("Error occurred during receiving message. Exception: {}, Session: {}, Message: {}", ex,
                         this.getSession(), message);
        }
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        LOGGER.debug("WS Close: {} Reason: {}", statusCode, reason);
        try {
            ActorUtils.getClientSupervisor(context).tell(new SessionCloseEvent(this.getSession()),
                                                         ActorRef.noSender());
        } catch (Exception ex) {
            LOGGER.error("Error occurred during disconnection. Exception: {}, Session: {}", ex,
                         this.getSession());
        }
    }

    @Override
    public void onWebSocketError(Throwable cause) {
        LOGGER.debug("WS Error: Reason: {}, Session: {}", cause, this.getSession());
    }
}
