package com.dfn.rm.connector.httpserver;

import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.StatusCodes;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import com.dfn.rm.util.settings.Settings;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletionStage;


/**
 * Http Server.
 */

public class HttpServer {

    private static final HttpServer INSTANCE = new HttpServer();
    private static final String IMAGE_DIRECTRY = "filesystem" + File.separator
            + "imgdirectry" + File.separator + "Questions" + File.separator + "images";
    private static final String CV_DIRECTRY = "filesystem" + File.separator + "UserRegistration" + File.separator
            + "CV";
    private static final String PAPER_DIRECTRY = "filesystem" + File.separator + "exampaper" + File.separator
            + "user" + File.separator + "paper";

    public static HttpServer getInstance() {
        return INSTANCE;
    }

    public void create(ActorSystem system) {
        try {
            final Materializer materializer = ActorMaterializer.create(system);
            CompletionStage<ServerBinding> serverBindingFuture =
                    Http.get(system).bindAndHandleSync(
                            request -> {
                                String url = request.getUri().path();
                                if (url.contains("jpg") || url.contains("png") || url.contains("gif")) {
                                    return getFileIfAvailable(IMAGE_DIRECTRY, request);
                                } else if (url.contains("pdf") && !url.contains("DFN_")) {
                                    return getFileIfAvailable(CV_DIRECTRY, request);
                                } else if (request.getUri().path().contains("DFN_")) {
                                    return getFileIfAvailable(PAPER_DIRECTRY, request);
                                } else {
                                    request.discardEntityBytes(materializer);
                                    return HttpResponse.create().withStatus(
                                            StatusCodes.NOT_FOUND).withEntity("Unknown resource!");
                                }
                            }, ConnectHttp.toHost(Settings.getInstance().getHttpServerSettings().getIp(),
                                    Integer.valueOf(Settings.getInstance().getHttpServerSettings().getPort())),
                            materializer);

            System.in.read();
            serverBindingFuture
                    .thenCompose(ServerBinding::unbind)
                    .thenAccept(unbound -> system.terminate());

        } catch (RuntimeException e) {
            system.terminate();
        } catch (IOException e) {
            system.terminate();
        }
    }

    public HttpResponse getFileIfAvailable(String path, HttpRequest request) {
        if (new File(path).exists()) {
            return HttpResponse.create().withEntity(
                    ContentTypes.NO_CONTENT_TYPE, new File(
                            path + request.getUri().path()));
        } else {
            return HttpResponse.create().withStatus(
                    StatusCodes.NOT_FOUND).withEntity("Unknown resource!");
        }
    }
}

