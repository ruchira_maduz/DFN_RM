package com.dfn.rm.datastore;

import com.dfn.rm.connector.websocket.messages.RequestHeader;

import java.util.HashMap;

/**
 * Data Store to track Headers .
 */
public final class RequestHeaderDataStore {

    private static final RequestHeaderDataStore INSTANCE = new RequestHeaderDataStore();

    private HashMap<String, RequestHeader> dataStore = new HashMap<>();

    private RequestHeaderDataStore() {

    }

    public static RequestHeaderDataStore getSharedInstance() {
        return INSTANCE;
    }

    public void putData(String id, RequestHeader data) {
        dataStore.put(id, data);
    }

    public RequestHeader getData(String id) {
        return dataStore.get(id);
    }

    public void removeData(String id) {
        this.dataStore.remove(id);
    }
}
