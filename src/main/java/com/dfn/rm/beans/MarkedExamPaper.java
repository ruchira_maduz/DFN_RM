package com.dfn.rm.beans;

import java.util.List;

/**
 * MarkedExamPaper class used to store MarkedExamPaper.
 */

public class MarkedExamPaper {

    private List<MarkedQuestion> answers;

    public List<MarkedQuestion> getQuestions() {
        return answers;
    }

    public void setQuestions(List<MarkedQuestion> questionsList) {
        this.answers = questionsList;
    }
}
