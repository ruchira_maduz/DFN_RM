package com.dfn.rm.beans;

/**
 * Show Paper  Class used to send paper for each student record.
 */

public class ShowPaper {

    private String userID;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
