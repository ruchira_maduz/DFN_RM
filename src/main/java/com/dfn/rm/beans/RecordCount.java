package com.dfn.rm.beans;

/**
 * Record Count Class used to send student search record count.
 */


public class RecordCount {

    private long searchRecordCount;

    public RecordCount(long searchRecordCount) {
        this.searchRecordCount = searchRecordCount;
    }

    public long getSearchRecordCount() {
        return searchRecordCount;
    }

    public void setSearchRecordCount(long searchRecordCount) {
        this.searchRecordCount = searchRecordCount;
    }
}
