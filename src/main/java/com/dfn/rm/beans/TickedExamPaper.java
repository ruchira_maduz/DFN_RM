package com.dfn.rm.beans;

import java.util.List;

/**
 * TickedExamPaper class used to store MarkedExamPaper.
 */

public class TickedExamPaper {

    private List<TickedQuestions> answers;

    public List<TickedQuestions> getQuestions() {
        return answers;
    }

    public void setQuestions(List<TickedQuestions> questionsList) {
        this.answers = questionsList;
    }
}
