package com.dfn.rm.beans;

import java.util.Date;

/**
 * ExamAgreement class used to pass ExamDetails to client.
 */

public class ExamAgreement {

    private String position;
    private String duration;
    private String description;
    private String honorCode;
    private String examId;
    private Date expireTime;
    private int noOfQuestions;

    public ExamAgreement(String position, String duration, String examId, String description,
                         String honorCode, int noOfQuestions) {
        this.setPosition(position);
        this.setDuration(duration);
        this.setHonorCode(honorCode);
        this.setDescription(description);
        this.setExamId(examId);
        this.setNoOfQuestions(noOfQuestions);
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHonorCode() {
        return honorCode;
    }

    public void setHonorCode(String honorCode) {
        this.honorCode = honorCode;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public int getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(int noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }
}
