package com.dfn.rm.beans;

import java.io.Serializable;

/**
 * Answer class for Process ExamPaper.
 */

public class Answer implements Serializable {

    private static final long serialVersionUID = 10L;

    private long id;
    private int type;
    private String text;
    private String url;
    private int questionId;
    private double weight;


    public Answer() {

    }

    public Answer(long id, int type, String url, int questionId) {
        this.id = id;
        this.type = type;
        this.url = url;
        this.questionId = questionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

}
