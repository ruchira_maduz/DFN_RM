package com.dfn.rm.beans;

import java.util.List;

/**
 * MarkedQuestion class used to generate MarkedExamPaper.
 */

public class MarkedQuestion {
    private int questionID;

    private List<Object> answerID;

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public List<Object> getAnswerIDs() {
        return answerID;
    }

    public void setAnswerIDs(List<Object> answerID) {
        this.answerID = answerID;
    }
}
