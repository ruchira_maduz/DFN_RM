package com.dfn.rm.beans;

/**
 * difficultyConfig class used to generate ExamPaper.
 */

public class DifficultyConfig {

    private long id;
    private int iqQuestions;
    private int techQuestions;
    private int diffHardQuestions;
    private int diffMediumQuestions;
    private int diffEasyQuestions;
    private String tags;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIqQuestions() {
        return iqQuestions;
    }

    public void setIqQuestions(int iqQuestions) {
        this.iqQuestions = iqQuestions;
    }

    public int getTechQuestions() {
        return techQuestions;
    }

    public void setTechQuestions(int techQuestions) {
        this.techQuestions = techQuestions;
    }

    public int getDiffHardQuestions() {
        return diffHardQuestions;
    }

    public void setDiffHardQuestions(int diffHardQuestions) {
        this.diffHardQuestions = diffHardQuestions;
    }

    public int getDiffMediumQuestions() {
        return diffMediumQuestions;
    }

    public void setDiffMediumQuestions(int diffMediumQuestions) {
        this.diffMediumQuestions = diffMediumQuestions;
    }

    public int getDiffEasyQuestions() {
        return diffEasyQuestions;
    }

    public void setDiffEasyQuestions(int diffEasyQuestions) {
        this.diffEasyQuestions = diffEasyQuestions;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
