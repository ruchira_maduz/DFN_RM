package com.dfn.rm.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Question class used to generate ExamPaper.
 */

public class Question implements Serializable {

    private static final long serialVersionUID = 20L;

    private long id;
    private int type;
    private int difficulty;
    private String body;
    private String url;
    private String title;
    private int category;
    private List<Answer> answer;


    public Question() {

    }

    public Question(long id, int type, int difficulty, String body, String url,
                    String title, int category, List<Answer> answers) {
        this.id = id;
        this.type = type;
        this.difficulty = difficulty;
        this.body = body;
        this.url = url;
        this.title = title;
        this.category = category;
        this.answer = answers;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }
}
