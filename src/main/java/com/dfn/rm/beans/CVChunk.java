package com.dfn.rm.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Cv chunk class used to store each chunk deatils of cv string.
 */

public class CVChunk {
    @SerializedName("length")
    private long totalBytes;

    @SerializedName("chunkLength")
    private long chunkBytes;

    @SerializedName("index")
    private int index;

    @SerializedName("chunks")
    private int chunkCount;

    @SerializedName("array")
    private String byteString;

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public long getChunkBytes() {
        return chunkBytes;
    }

    public void setChunkBytes(long chunkBytes) {
        this.chunkBytes = chunkBytes;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getByteString() {
        return byteString;
    }

    public void setByteString(String byteString) {
        this.byteString = byteString;
    }

    public int getChunkCount() {
        return chunkCount;
    }

    public void setChunkCount(int chunkCount) {
        this.chunkCount = chunkCount;
    }
}
