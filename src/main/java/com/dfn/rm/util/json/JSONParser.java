package com.dfn.rm.util.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

/**
 * Common JSON Parser class for maintaining single json parsing rules throughout Recruitement Manager.
 */
public final class JSONParser implements JSONUtils {

    private static final Gson GSON = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .setLongSerializationPolicy(LongSerializationPolicy.STRING)
            .create();
    private static final JSONParser INSTANCE = new JSONParser();

    private JSONParser() {

    }

    public static JSONParser getInstance() {
        return INSTANCE;
    }

    @Override
    public String toJson(Object data) {
        return GSON.toJson(data);
    }

    @Override
    public <T> String toJson(Object data, Class<T> classOfT) {
        return GSON.toJson(data, classOfT);
    }

    @Override
    public <T> T fromJson(String json, Class<T> classOfT) {
        return GSON.fromJson(json, classOfT);
    }
}
