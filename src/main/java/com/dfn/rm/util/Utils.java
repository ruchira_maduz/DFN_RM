/**
 * Utility package of Recruitment Manager Server
 */
package com.dfn.rm.util;

import com.dfn.rm.util.json.JSONParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Main Utility class for performing various common util actions.
 */
public final class Utils {

    private static final Logger LOGGER = LogManager.getLogger(Utils.class);

    public static <T extends Object> T getMessageInstance(Object message, Class<T> type) {
        String jsonString = JSONParser.getInstance().toJson(message, Map.class);
        return JSONParser.getInstance().fromJson(jsonString, type);
    }

    public static String getString(String base64) {
        String partSeparator = ",";
        if (base64.contains(partSeparator)) {
            return base64.split(partSeparator)[1];
        } else {
            return base64;
        }
    }

    public static boolean isNullOrEmptyString(String string) {
        return string == null || string.trim().isEmpty();
    }

    private Utils() {

    }

    public static String formatField(String input) {
        if (input != null) {
            return input.trim().isEmpty() ? null : input.trim();
        } else {
            return null;
        }
    }

    public static int getPageNo(int pagination) {
        return (pagination * Constants.SEARCH_COUNT_PER_PAGE);
    }


}
