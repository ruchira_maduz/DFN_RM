package com.dfn.rm.util;

import akka.actor.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Utility class related to handling Actor System based utility functions.
 */
public final class ActorUtils {

    private static final ThreadLocalRandom LOCAL_RANDOM_GENERATOR = ThreadLocalRandom.current();
    private static final Logger LOGGER = LogManager.getLogger(ActorUtils.class);
    private static final String SYSTEM_PATH = "/user/";


    private static ActorRef examsSupervisor;
    private static ActorRef clientSupervisor;
    private static AtomicLong atomicLong = new AtomicLong();

    private ActorUtils() {

    }

    public static String generateUniqueId() {
        long timeID = System.currentTimeMillis();
        long uniqueID = atomicLong.incrementAndGet();
        String nodeID = "A";
        return "" + nodeID + "" + timeID + "" + uniqueID;
    }

    /**
     * @param actorContext - Actor Context of the parent actor.
     * @param actorName    - Child Actors Name.
     * @param maxActorId   - Maximum number of worker actors for this category.
     * @return - Child Actor Reference for given name.
     */
    public static ActorRef getChildActor(AbstractActor.ActorContext actorContext, ActorName actorName,
                                         int maxActorId) {
        int actorId = LOCAL_RANDOM_GENERATOR.nextInt(0, maxActorId != 0
                ? maxActorId
                : 1);
        Optional<ActorRef> childActor = actorContext.findChild(actorName.toString() + "-" + actorId);
        return childActor.orElse(null);
    }

    /**
     * @param actorContext - Actor Context of the parent actor.
     * @param actorName    - Child Actors Name.
     * @param actorSuffix  - Suffix of the child actor for this parent actor. Ex: Exchange suffix: TDWL, DFM
     *                     Symbol suffix: 1010, 1020 etc.
     * @return - Child Actor Reference for given name.
     */
    public static ActorRef getChildActor(AbstractActor.ActorContext actorContext, ActorName actorName,
                                         String actorSuffix) {
        Optional<ActorRef> childActor = actorContext.findChild(actorName.toString() + "-" + actorSuffix);
        return childActor.orElse(null);
    }

    /**
     * @param actorContext - Actor Context of the parent actor.
     * @param actorName    - Child Actors Name.
     * @return - Child Actor Reference for given name.
     */
    public static ActorRef getChildActor(AbstractActor.ActorContext actorContext, ActorName actorName) {
        Optional<ActorRef> childActor = actorContext.findChild(actorName.toString());
        return childActor.orElse(null);
    }

    /**
     * Returns a specified supervisor actor of a given context.
     *
     * @param actorContext - Actor context.
     * @param actorName    - Actor name of the supervisor.
     * @return - Actor selection which can be used to tell messages to.
     */
    private static ActorSelection getSupervisor(ActorContext actorContext, ActorName actorName) {
        return actorContext.actorSelection(SYSTEM_PATH
                                                   + ActorName.RECRUITMENT_SUPERVISOR.toString()
                                                   + "/"
                                                   + actorName.toString());
    }

    /**
     * Returns a ActorRef of Exchange Supervisor actors.
     *
     * @param actorContext - Actor context.
     * @return - Actor Reference which can be used to tell messages to.
     */
    public static ActorRef getExamsSupervisor(ActorContext actorContext) {
        if (examsSupervisor != null) {
            return examsSupervisor;
        } else {
            LOGGER.warn("Exams Supervisor may not have initialized properly. Trying to get by reference.");
            ActorSelection examsSupervisorSelection = getSupervisor(actorContext, ActorName.EXAMS_SUPERVISOR);
            ActorRef actorRef = resolveActorRefSync(examsSupervisorSelection);
            examsSupervisor = actorRef;
            return actorRef;
        }
    }

    /**
     * Returns a ActorRef of Fix Supervisor actors.
     *
     * @param actorContext - Actor context.
     * @return - Actor Reference which can be used to tell messages to.
     */
    public static ActorRef getClientSupervisor(ActorContext actorContext) {
        if (clientSupervisor != null) {
            return clientSupervisor;
        } else {
            LOGGER.warn("Client Supervisor may not have initialized properly. Trying to get by reference.");
            ActorSelection clientSupervisorSelection = getSupervisor(actorContext, ActorName.CLIENT_SUPERVISOR);
            ActorRef actorRef = resolveActorRefSync(clientSupervisorSelection);
            clientSupervisor = actorRef;
            return actorRef;
        }
    }

    /**
     * Resolves one ActorRef from a given actor selection.
     *
     * @param actorSelection - Actor Selection.
     * @return - Actor Reference.
     */
    private static ActorRef resolveActorRefSync(ActorSelection actorSelection) {
        ActorRef actorRef = ActorRef.noSender();
        try {
            actorRef = actorSelection
                    .resolveOne(Duration.ofMillis(500)).toCompletableFuture().get();
        } catch (ExecutionException e) {
            LOGGER.error(e);
        } catch (InterruptedException e) {
            LOGGER.error(e);
            Thread.currentThread().interrupt();
        }
        return actorRef;
    }

    /**
     * Get a reference to all child actors of this actor. Can be used to broadcast messages
     * to all child actors.
     *
     * @param actorContext - Actor context of the parent actor.
     * @return - Reference for all child actors of the parent.
     */
    public static ActorSelection getChildActors(ActorContext actorContext) {
        String actorPath = ((ActorCell) actorContext).getSelf().path().toString();
        return actorContext.actorSelection(actorPath + "/*");
    }

    public static int generateRandomNumber(int start, int end) {
        return LOCAL_RANDOM_GENERATOR.nextInt(start, end != 0
                ? end
                : 1);
    }

    public static long generateRandomNumber(long start, long end) {
        return LOCAL_RANDOM_GENERATOR.nextLong(start, end != 0
                ? end
                : 1);
    }

    public static void setExamsSupervisor(ActorRef examsSupervisor) {
        ActorUtils.examsSupervisor = examsSupervisor;
    }

    public static void setClientSupervisor(ActorRef clientSupervisor) {
        ActorUtils.clientSupervisor = clientSupervisor;
    }

    public static void invalidateSupervisorRef() {
        examsSupervisor = null;
        clientSupervisor = null;
    }
}
