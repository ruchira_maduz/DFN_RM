package com.dfn.rm.util;


import com.dfn.rm.beans.User;
import com.dfn.rm.beans.UserRegistration;
import com.dfn.rm.util.security.PasswordEncrypter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * UserRegistration Utility class for performing various UserRegistration util actions.
 */

public final class UserRegUtils {

    private static final Logger LOGGER = LogManager.getLogger(UserRegUtils.class);


    public static User generateUserCredentials(UserRegistration userRegistration) {

        User user = null;
        try {
            String userName = generateRandomUserName(userRegistration.getName());
            String salt = getRandomString(5);
            String password = getRandomString(5);
            String passwordHash = generateRandomPasswordHash(salt, password);

            user = new User();
            user.setUserName(userName);
            user.setPassword(password);
            user.setPasswordHash(passwordHash);
            user.setSalt(salt);
            user.setUserType(Constants.USER_CAT_EXAM);
            user.setFullName(userRegistration.getName());
        } catch (Exception e) {
            LOGGER.error(e);
        }

        return user;
    }

    public static String getRandomString(int length) {
        return UUID.randomUUID().toString().substring(0, length);
    }

    public static String getFirstWord(String text) {
        return text.contains(" ") ? text.split(" ")[0] : text;
    }

    public static String generateRandomUserName(String name) {
        return getFirstWord(name).toLowerCase() + "_" + getRandomString(5);
    }

    public static String generateRandomPasswordHash(String salt, String password) {
        byte[] saltArr = salt.getBytes();
        return PasswordEncrypter.getsha256Securepassword(password, saltArr);
    }

    public static String generatePaperID(String userID) {
        return "DFN" + "_" + userID + "_" + getRandomString(5);
    }

    private UserRegUtils() {

    }

    public static List<String> generatePanelMailList(String panelString) {
        return Arrays.asList(panelString.replaceAll("\\s", "").split(","));
    }

}
