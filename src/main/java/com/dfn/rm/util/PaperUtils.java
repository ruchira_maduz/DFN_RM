package com.dfn.rm.util;

import com.dfn.rm.beans.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Paper Utility class for performing various paper util actions.
 */

public final class PaperUtils {

    private static final Logger LOGGER = LogManager.getLogger(PaperUtils.class);

    public static TickedExamPaper generateMarkedQuestions(List<ExamAnswer> examAnswerList) {
        TickedExamPaper markedExamPaper = new TickedExamPaper();
        try {
            List<TickedQuestions> markedQuestionsList = new ArrayList<>();
            int questionId = -1;
            List<String> markedAnswersList = null;
            for (ExamAnswer examAnswer : examAnswerList) {
                if (questionId != examAnswer.getQuestionID()) {
                    questionId = examAnswer.getQuestionID();
                    TickedQuestions markedQuestions = new TickedQuestions();
                    markedAnswersList = new ArrayList<>();
                    markedQuestions.setQuestionID(questionId);
                    markedQuestions.setAnswerIDs(markedAnswersList);
                    markedQuestionsList.add(markedQuestions);
                }
                markedAnswersList.add(examAnswer.getAnswerID());
            }

            markedExamPaper.setQuestions(markedQuestionsList);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return markedExamPaper;
    }

    public static boolean isNumeric(Object text) {
        if (StringUtils.isNumeric((String) text)) {
            return true;
        }
        return false;
    }

    public static Answer filterAnswerList(List<Answer> answerList, Object selectedAnswer, int category) {
        Answer filteredAnswer = null;
        try {
            if (category == Constants.QUESTION_TYPE_SHORT_ANSWERS) {
                if (!isNumeric(selectedAnswer)) {
                    filteredAnswer = answerList.stream().filter(
                            answer -> ((String) selectedAnswer).equals(answer.getText()))
                            .findAny()
                            .orElse(new Answer());
                } else {
                    filteredAnswer = answerList.stream().filter(
                            answer -> Double.valueOf((String) selectedAnswer).doubleValue()
                                    == Double.valueOf(answer.getText()).doubleValue())
                            .findAny()
                            .orElse(new Answer());
                }
            } else {
                filteredAnswer = answerList.stream()
                        .filter(answer -> Integer.valueOf((String) selectedAnswer).intValue() == answer.getId())
                        .findAny()
                        .orElse(new Answer());
            }
        } catch (Exception e) {
            LOGGER.error(e);
            filteredAnswer = new Answer();
        }
        return filteredAnswer;
    }

    public static List<Question> generatePaperQuestionList(TickedExamPaper markedExamPaper,
                                                           List<Question> allQuestions, List<Answer> allAnswers) {

        List<Question> paperQuestionList = new ArrayList<>();

        for (TickedQuestions tickedQuestions : markedExamPaper.getQuestions()) {
            Question question = allQuestions.stream().filter(
                    x -> x.getId() == tickedQuestions.getQuestionID()).findAny().orElse(null);

            if (question != null) {
                paperQuestionList.add(question);
                List<Answer> answerList = new ArrayList<>();
                question.setAnswer(answerList);
                for (String id : tickedQuestions.getAnswerIDs()) {
                    Answer answer = null;
                    if (question.getCategory() == Constants.QUESTION_TYPE_MCQ) {
                        answer = allAnswers.stream().filter(
                                y -> y.getId() == Integer.valueOf(id)).findAny().orElse(null);
                    } else {
                        answer = new Answer();
                        answer.setText(id);
                    }
                    if (answer != null) {
                        answerList.add(answer);
                    }
                }
            }

        }
        return paperQuestionList;
    }

    public static List<Answer> filteredAnswerList(List<Answer> answerList) {
        List<Answer> filteredList = new ArrayList<>();
        try {
            filteredList.add(new Answer(answerList.get(0).getId(), answerList.get(0).getType(),
                    answerList.get(0).getUrl(), answerList.get(0).getQuestionId()));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return filteredList;
    }

    private PaperUtils() {

    }
}
