package com.dfn.rm.util.settings;

/**
 * This class represents the main settings object which encapsulates all the settings defined by admin users.
 */

public class Settings {

    private static final Settings INSTANCE = new Settings();

    private DBSettings dbSettings;

    private ClientSettings clientSettings;

    private HttpServerSettings httpServerSettings;

    public static Settings getInstance() {
        return INSTANCE;
    }

    public DBSettings getDbSettings() {
        return dbSettings;
    }

    public void setDbSettings(DBSettings dbSettings) {
        this.dbSettings = dbSettings;
    }

    public ClientSettings getClientSettings() {
        return clientSettings;
    }

    public void setClientSettings(ClientSettings clientSettings) {
        this.clientSettings = clientSettings;
    }

    public HttpServerSettings getHttpServerSettings() {
        return httpServerSettings;
    }

    public void setHttpServerSettings(HttpServerSettings httpServerSettings) {
        this.httpServerSettings = httpServerSettings;
    }
}
