package com.dfn.rm.util.settings;

/**
 * Settings class for Server HttpServer settings of Recruitement Manager.
 */

public class HttpServerSettings {

    /**
     * IP address of Server httpServerSettings.
     */
    private String ip;

    /**
     * Web Socket Port of Server httpServerSettings.
     */
    private String port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
