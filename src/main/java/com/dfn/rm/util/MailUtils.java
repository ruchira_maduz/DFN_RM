package com.dfn.rm.util;

import com.dfn.rm.beans.User;
import com.dfn.rm.beans.UserRegistration;
import com.dfn.rm.email.ContactMail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Mail Utility class for performing various Mail util actions.
 */

public final class MailUtils {

    private static final Logger LOGGER = LogManager.getLogger(MailUtils.class);
    private static final DateFormat TIME_OUTPUT_FORMAT = new SimpleDateFormat("HH:mm");
    private static final DateFormat DATE_OUTPUT_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat INPUT_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public static String replaceRegistrationFields(String text, ContactMail contactMail) {
        String resultText = null;
        try {
            resultText = text.replaceAll("#User#", contactMail.getModel().get("User").toString());
            resultText = resultText.replaceAll("#Date#", contactMail.getModel().get("Date").toString());
            resultText = resultText.replaceAll("#Time#", contactMail.getModel().get("Time").toString());
            resultText = resultText.replaceAll("#UserName#", contactMail.getModel().get("UserName").toString());
            resultText = resultText.replaceAll("#Password#", contactMail.getModel().get("Password").toString());
        } catch (Exception e) {
            LOGGER.error(e);
        }

        return resultText;
    }

    public static String replacePanelFields(String text, ContactMail contactMail) {
        String resultText = null;
        try {
            resultText = text.replaceAll("#FullName#", contactMail.getModel().get("FullName").toString());
            resultText = resultText.replaceAll("#NIC#", contactMail.getModel().get("NIC").toString());
            resultText = resultText.replaceAll("#Email#", contactMail.getModel().get("Email").toString());
            resultText = resultText.replaceAll("#Department#",
                    contactMail.getModel().get("Department").toString());
            resultText = resultText.replaceAll("#Position#", contactMail.getModel().get("Position").toString());
            resultText = resultText.replaceAll("#Marks#", contactMail.getModel().get("Marks").toString());
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return resultText;
    }

    public static Map addToRegistrationMap(UserRegistration userRegistration, User user) {
        Map map = new HashMap();

        try {
            map.put("User", userRegistration.getName().contains(" ")
                    ? userRegistration.getName().split(" ")[0] : userRegistration.getName());
            map.put("Date", userRegistration.getTechIntviewDate());
            map.put("Email", userRegistration.getEmail());
            map.put("Time", userRegistration.getTechIntviewTime());
            map.put("UserName", user.getUserName());
            map.put("Password", user.getPassword());
        } catch (Exception e) {
            LOGGER.error(e);
        }

        return map;
    }

    public static Map addToPanelMap(UserRegistration userRegistration, double marks) {
        Map map = new HashMap();

        try {
            map.put("FullName", userRegistration.getName());
            map.put("NIC", userRegistration.getNic());
            map.put("Email", userRegistration.getEmail());
            map.put("Department", userRegistration.getDepartment());
            map.put("Position", userRegistration.getPosition());
            map.put("Marks", marks);
        } catch (Exception e) {
            LOGGER.error(e);
        }

        return map;
    }

    public static String formatTime(String dateString) {
        Date date = null;
        try {
            date = INPUT_FORMAT.parse(dateString);

        } catch (ParseException e) {
            LOGGER.error(e);
        }
        return TIME_OUTPUT_FORMAT.format(date);
    }

    public static String formatDate(String dateString) {
        Date date = null;
        try {
            date = INPUT_FORMAT.parse(dateString);
        } catch (ParseException e) {
            LOGGER.error(e);
        }
        return DATE_OUTPUT_FORMAT.format(date);
    }

    private MailUtils() {

    }
}
