package com.dfn.rm.exam;

import com.dfn.rm.beans.Answer;
import com.dfn.rm.beans.DifficultyConfig;
import com.dfn.rm.beans.ExamPaper;
import com.dfn.rm.beans.Question;
import com.dfn.rm.connector.database.DBHandler;
import com.dfn.rm.connector.database.DBUtilStore;
import org.jdbi.v3.core.Handle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.dfn.rm.util.Constants.*;

/**
 * Mix Exam paper Generator .
 */

public class MixPaperGenerator extends CommonPaperGenerator {

    private String position;
    private int techQuestions;
    private int diffHardQuestions;
    private int diffMediumQuestions;
    private int diffEasyQuestions;

    public MixPaperGenerator(String position, int examCategory) {
        super(examCategory);
        this.position = position;
    }

    public ExamPaper generateMixPaper() {
        List<Question> questions = generateMixtureOfQuestions();
        examPaper.setQuestions(questions);
        return examPaper;
    }

    private void setDifficultyLevel(String position, Handle handle) {
        DifficultyConfig difficultyConfig = DBHandler.getInstance().getDifficultyConfig(position, handle);
        setDiffHardQuestions(difficultyConfig.getDiffHardQuestions());
        setDiffMediumQuestions(difficultyConfig.getDiffMediumQuestions());
        setDiffEasyQuestions(difficultyConfig.getDiffEasyQuestions());
        setIqQuestions(difficultyConfig.getIqQuestions());
        setTechQuestions(difficultyConfig.getTechQuestions());
    }

    public List<Question> generateEasyQuestionList(List<Question> allQuestionsList, Handle handle) {
        List<Question> generatedEasyQuestions = new ArrayList<>();

        try {
            List<Question> allEasyQuestions = allQuestionsList.stream().filter(
                    question -> question.getDifficulty() == DIFFICULTY_CONFIG_EASY).collect(Collectors.toList());
            for (int i = 0; i < getDiffEasyQuestions(); i++) {
                int id = RANDOM_NUMBER_GENERATOR.nextInt(allEasyQuestions.size());
                Question question = allEasyQuestions.get(id);
                List<Answer> answerList = DBHandler.getInstance().getAnswersOfQuestion(question.getId(), handle);
                if (answerList != null) {
                    question.setAnswer(answerList);
                }
                generatedEasyQuestions.add(question);
                allEasyQuestions.remove(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return generatedEasyQuestions;
    }

    public List<Question> generateMediumQuestionList(List<Question> allQuestionsList, Handle handle) {
        List<Question> generatedMediumQuestions = new ArrayList<>();

        try {
            List<Question> allMediumyQuestions = allQuestionsList.stream().filter(
                    question -> question.getDifficulty() == DIFFICULTY_CONFIG_MEDIUM).collect(Collectors.toList());
            for (int i = 0; i < getDiffMediumQuestions(); i++) {
                int id = RANDOM_NUMBER_GENERATOR.nextInt(allMediumyQuestions.size());
                Question question = allMediumyQuestions.get(id);
                List<Answer> answerList = DBHandler.getInstance().getAnswersOfQuestion(question.getId(), handle);
                if (answerList != null) {
                    question.setAnswer(answerList);
                }
                generatedMediumQuestions.add(question);
                allMediumyQuestions.remove(id);
            }
        } catch (Exception e) {

        }
        return generatedMediumQuestions;
    }

    public List<Question> generateHardQuestionList(List<Question> allQuestionsList, Handle handle) {
        List<Question> generatedHardQuestions = new ArrayList<>();

        try {
            List<Question> allHardQuestions = allQuestionsList.stream().filter(
                    question -> question.getDifficulty() == DIFFICULTY_CONFIG_HARD).collect(Collectors.toList());
            for (int i = 0; i < getDiffMediumQuestions(); i++) {
                int id = RANDOM_NUMBER_GENERATOR.nextInt(allHardQuestions.size());
                Question question = allHardQuestions.get(id);
                List<Answer> answerList = DBHandler.getInstance().getAnswersOfQuestion(question.getId(), handle);
                if (answerList != null) {
                    question.setAnswer(answerList);
                }
                generatedHardQuestions.add(question);
                allHardQuestions.remove(id);
            }
        } catch (Exception e) {

        }
        return generatedHardQuestions;
    }

    public List<Question> generateTechnicalQuestions() {
        List<Question> technicalQuestionsList = null;
        try (Handle handle = DBUtilStore.getInstance().getDbiInstance().open()) {
            setDifficultyLevel(position, handle);
            List<Question> allQuestionsList = DBHandler.getInstance().getQuestionsFromCategory(
                    EXAM_CATEGORY_TECHNICAL, handle);
            technicalQuestionsList = Stream.of(generateEasyQuestionList(allQuestionsList, handle),
                    generateMediumQuestionList(allQuestionsList, handle),
                    generateHardQuestionList(allQuestionsList, handle))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

        } catch (Exception e) {

        }
        return technicalQuestionsList;
    }

    private List<Question> generateMixtureOfQuestions() {
        List<Question> iqQuestionsList = generateIQQuestions();
        List<Question> technicalQuestionsList = generateTechnicalQuestions();

        return Stream.of(iqQuestionsList, technicalQuestionsList).flatMap(
                Collection::stream).collect(Collectors.toList());
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getTechQuestions() {
        return techQuestions;
    }

    public void setTechQuestions(int techQuestions) {
        this.techQuestions = techQuestions;
    }

    public int getDiffHardQuestions() {
        return diffHardQuestions;
    }

    public void setDiffHardQuestions(int diffHardQuestions) {
        this.diffHardQuestions = diffHardQuestions;
    }

    public int getDiffMediumQuestions() {
        return diffMediumQuestions;
    }

    public void setDiffMediumQuestions(int diffMediumQuestions) {
        this.diffMediumQuestions = diffMediumQuestions;
    }

    public int getDiffEasyQuestions() {
        return diffEasyQuestions;
    }

    public void setDiffEasyQuestions(int diffEasyQuestions) {
        this.diffEasyQuestions = diffEasyQuestions;
    }
}
