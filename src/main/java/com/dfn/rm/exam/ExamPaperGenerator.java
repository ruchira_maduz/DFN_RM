package com.dfn.rm.exam;

import com.dfn.rm.beans.ExamPaper;

/**
 * Exam paper Generator interface .
 */

public interface ExamPaperGenerator {

    ExamPaper generate();

}
