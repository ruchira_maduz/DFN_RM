package com.dfn.rm;

import com.dfn.rm.util.ActorName;

public class ActorBasedTestBase extends RecrutmentManagerTestBase {
    private static final String SYSTEM_PATH = "/user/"
            + ActorName.RECRUITMENT_SUPERVISOR.toString()
            + "/";
    protected static final String CLIENT_SUPERVISOR_PATH = SYSTEM_PATH + ActorName.CLIENT_SUPERVISOR;
    protected static final String EXAMS_SUPERVISOR_PATH = SYSTEM_PATH + ActorName.EXAMS_SUPERVISOR;

}
