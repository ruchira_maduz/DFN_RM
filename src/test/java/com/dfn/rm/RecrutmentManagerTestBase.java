package com.dfn.rm;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import com.dfn.rm.actors.RecruitmentSupervisor;
import com.dfn.rm.connector.database.DBContentHandler;
import com.dfn.rm.util.ActorName;
import com.dfn.rm.util.SettingsNotFoundException;
import com.dfn.rm.util.SettingsReader;
import com.dfn.rm.util.settings.Settings;
import com.typesafe.config.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

public class RecrutmentManagerTestBase {
    /**
     * Default settings file for athena server.
     */
    protected static final String DEFAULT_SETTINGS_FILE = "src/test/resources/config/SettingsTest.yml";
    protected static final String DEFAULT_AKKA_SETTINGS_FILE = "src/test/resources/config/AkkaTest.conf";
    protected static ActorSystem algoEngine;
    private static final Logger LOGGER = LogManager.getLogger(RecrutmentManagerTestBase.class);

    @BeforeClass
    public static void loadSettings() throws InterruptedException, SettingsNotFoundException {
        Settings settings = SettingsReader.loadSettings(DEFAULT_SETTINGS_FILE);
        Settings.getInstance().setDbSettings(settings.getDbSettings());
        Settings.getInstance().setClientSettings(settings.getClientSettings());
        Config config = SettingsReader.loadAkkaSettings(DEFAULT_AKKA_SETTINGS_FILE);
        String dbType = Settings.getInstance().getDbSettings().getDatabaseType();
        if ("h2".equalsIgnoreCase(dbType)) {
            LOGGER.info("Creating Default DB Schema: {}", dbType);
            try {
                DBContentHandler.getInstance().initializeDBContent();
            } catch (Exception e) {
                LOGGER.error(e);
            }
        } else {
            LOGGER.info("Ignoring Default Schema creation since Schema is already created: {}", dbType);
        }
        algoEngine = ActorSystem.create("AlgoEngine", config);
        // Start Athena actor system.
        algoEngine.actorOf(Props.create(RecruitmentSupervisor.class),
                           ActorName.RECRUITMENT_SUPERVISOR.toString());
        Thread.sleep(30000);
    }

    @AfterClass
    public static void shutdownActorSystem() {
        TestKit.shutdownActorSystem(algoEngine, Duration.apply(300, TimeUnit.SECONDS), true);
    }
}
