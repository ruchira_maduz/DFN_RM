package com.dfn.rm.actors.general;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.dfn.rm.ActorBasedTestBase;
import com.dfn.rm.actors.ClientActor;
import com.dfn.rm.actors.RecruitementActorTestKit;
import com.dfn.rm.connector.websocket.MessageType;
import com.dfn.rm.connector.websocket.events.SessionInfo;
import com.dfn.rm.connector.websocket.messages.LoginResponse;
import com.dfn.rm.connector.websocket.messages.RequestHeader;
import com.dfn.rm.connector.websocket.messages.ResponseHeader;
import com.dfn.rm.connector.websocket.messages.ResponseMessage;
import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientConnectorTest extends ActorBasedTestBase {

    @Test
    public void responseMessageTest() {
        ActorSystem actorSystem = algoEngine;
        RecruitementActorTestKit athenaActorTestKit = new RecruitementActorTestKit(algoEngine);

        Session mockSession = mock(Session.class);
        RemoteEndpoint mockRemote = mock(RemoteEndpoint.class);
        when(mockSession.getRemote()).thenReturn(mockRemote);
        SessionInfo sessionInfo = new SessionInfo("1", mockSession);

        ActorRef clientActor = actorSystem.actorOf(Props.create(ClientActor.class, sessionInfo),
                "TEST_CLIENT_CONN_ACTOR" + UUID.randomUUID().toString());

        try {
            ResponseMessage responseMessage = new ResponseMessage();
            ResponseHeader responseHeader = new ResponseHeader();
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setAuthStatus(1);
            responseHeader.setMessageType(MessageType.REQUEST_MSG_LOGIN);

            responseMessage.setHeader(responseHeader);
            responseMessage.setResObject(loginResponse);

            clientActor.tell(responseMessage, athenaActorTestKit.getProbe().getRef());
            athenaActorTestKit.getProbe().expectNoMessage();
//
//            responseHeader.setMessageType(MessageType.REQUEST_MSG_USER_REGISTRATION);
//
//            responseMessage.setHeader(responseHeader);
//
//            clientActor.tell(responseMessage, athenaActorTestKit.getProbe().getRef());
//            athenaActorTestKit.getProbe().expectNoMessage();

        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void handleSessionInfoTest() {
        ActorSystem actorSystem = algoEngine;
        RecruitementActorTestKit athenaActorTestKit = new RecruitementActorTestKit(algoEngine);

        Session mockSession = mock(Session.class);
        RemoteEndpoint mockRemote = mock(RemoteEndpoint.class);
        when(mockSession.getRemote()).thenReturn(mockRemote);
        SessionInfo sessionInfo = new SessionInfo("1", mockSession);

        ActorRef clientActor = actorSystem.actorOf(Props.create(ClientActor.class, sessionInfo),
                "TEST_CLIENT_CONN_ACTOR" + UUID.randomUUID().toString());

        try {
            clientActor.tell(sessionInfo, athenaActorTestKit.getProbe().getRef());
            athenaActorTestKit.getProbe().expectNoMessage();
        } catch (Exception e) {
            Assert.fail();
        }

    }

    private RequestHeader getTestRequestHeader() {
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMessageType(1);
        requestHeader.setChannelId(10);
        requestHeader.setClientVersion("1.0");
        requestHeader.setUserId("root");
        requestHeader.setSessionId("1");
        requestHeader.setClientIp("127.0.0.1");
        requestHeader.setTenantCode("1");
        requestHeader.setUnqReqId("1");
        return requestHeader;
    }

    private byte[] readPDF() {
        final String DEFAULT_PDF_FILE = "src/test/resources/pdf/test.pdf";
        File file = new File(DEFAULT_PDF_FILE);
        byte[] bytes = new byte[(int) file.length()];
        try (FileInputStream fis = new FileInputStream(file)) {
            bytes = FileUtils.readFileToByteArray(file);
        } catch (Exception e) {

        }
        return bytes;
    }

}
