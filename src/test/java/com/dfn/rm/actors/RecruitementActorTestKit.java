package com.dfn.rm.actors;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;

/**
 * Test probe for actor based tests in recruitementManager.
 */
public class RecruitementActorTestKit extends TestKit {
    public RecruitementActorTestKit(ActorSystem system) {
        super(system);
    }

    final TestKit probe = new TestKit(getSystem());

    public TestKit getProbe() {
        return probe;
    }

}
